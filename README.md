# GlyCosmos Layout

https://glycosmos-layout.glycosmos.org/

## js

```
<script src="https://glycosmos-layout.glycosmos.org/bundle.js"></script>
```

## glycosmos-table

https://docs.google.com/presentation/d/1ajYMDXQ_pgJ6J1UBytdj_LpHOUmlsLfM0LFlQ--W2Kc/edit?usp=sharing

# webpack

https://webpack.js.org/

## docs

https://webpack.js.org/concepts/

## GitHub

https://github.com/webpack/webpack

## up

```
docker-compose up -d
```

Now, open `http://localhost:8080` in your browser.

## LitElement

https://lit-element.polymer-project.org/

If you edit other files, you need to rebuild.

## build

```
docker-compose up -d --build
```

## production

### Build locally

```
docker-compose up -d --build
docker-compose run --rm node webpack --mode=production
mv public/bundle.js ../glycosmos-portal/app/assets/javascripts
```

### from GitLab Pages

```
wget https://glycosmos-layout.glycosmos.org/bundle.js
mv ./bundle.js ../glycosmos-portal/app/assets/javascripts
```
