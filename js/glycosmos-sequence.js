import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosSequence extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      uniprotId: { type: String },
      host: { type: String },
      positionArray: { type: Array, reflect: true },
      myArray: { type: Array, reflect: true },
      imageApi: { type: String },
      imageFile: { type: String }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "uniprot";
    this.uniprotId = "P02873";
    this.host = "";
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";
    this.imageFile = "https://image.beta.glycosmos.org/";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.positionArray = ["Loading..."];
    let positionUrl = this.domain + "glycosmos_uniprot_glycosylation?id=" + this.uniprotId;
    if (this.source == "gpdbs") {
      positionUrl = this.domain + "glycosmos_gpdbs_glycosylation?id=" + this.uniprotId;
    }
    fetch(positionUrl).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.positionArray = myJson;
    });
    this.myArray = ["Loading..."];
    let url = this.domain + "glycosmos_uniprot_sequence?id=" + this.uniprotId;
    if (this.source == "gpdbs") {
      url = this.domain + "glycosmos_gpdbs_sequence?id=" + this.uniprotId;
    }
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Sequence</div>
        <div class="card-body">
          ${this.cbodyTemplate}
        </div>
      </div>
    `;
  }

  get cbodyTemplate() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card">
            <div class="card-body">
              ${this.getSequence(this.myArray[0]['sequence'])}
            </div>
            <div class="card-footer text-muted">
              <span style="color:white; background-color:#a270ff; padding:2px;">N</span> : N-glycosylation Site<br>
              <span style="color:#384fff; font-weight:900;">___</span> : Potential Sequon
            </div>
          </div>
          ${this.glycosylationSites}
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }

  getSequence(aa_seq) {
    let positions = this.positionArray.map(pos => pos['position']);
    let site_array = positions.map(x => Number(x)).sort();
    let ami_len = aa_seq.length - 2,
      ps_array = [],
      fra3 = [];
    for (let i = 0 ; i < ami_len ; i++) {
      fra3 = aa_seq[i] + aa_seq[i+1] + aa_seq[i+2];
      if (/N[A-OQ-Z][ST]/.test(fra3)) {
        ps_array.push(i);
      }
    }
    let aa_array = aa_seq.split('');
    for (let site = 0 ; site < site_array.length ; site++) {
      aa_array[site_array[site]-1] = '<font style="color:white; background-color:#a270ff; padding:0 2px;">' + aa_array[site_array[site]-1] + '</font>';
    }
    for (let site = 0 ; site < ps_array.length ; site++ ) {
      aa_array[ps_array[site]] = '<span style="margin:0; padding:0; border-bottom:solid 3px #384fff;">' + aa_array[ps_array[site]];
      aa_array[ps_array[site]+2] = aa_array[ps_array[site]+2] + '</span>';
    }
    let sequence = aa_array.join("");
    return html`${unsafeHTML(sequence)}`;
  }

  get glycosylationSites() {
    if (this.positionArray.length) {
      if (this.positionArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">Glycosylation Sites</div>
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Position</th>
                    <th scope="col">Description</th>
                    <th scope="col">PubMed ID</th>
                    <th scope="col">Glycan</th>
                  </tr>
                </thead>
                <tbody>
                  ${this.positionArray.map((row, index) => html`
                    <tr>
                      <th scope="row">${index + 1}</th>
                      <td>${row['position']}</td>
                      <td>${row['description']}</td>
                      <td>
                        <ul style="padding-inline-start:20px;">
                          ${row['pubmed_ids'].map(pubmed_id => html`
                            ${pubmed_id? html`
                              <li><a href="https://www.ncbi.nlm.nih.gov/pubmed/${pubmed_id}" target="_blank">${pubmed_id}</a></li>
                            `:``}
                          `)}
                        </ul>
                      </td>
                      <td>
                        <div class="row">
                          ${row['gtc_ids'].map(gtc_id => html`
                            ${gtc_id? html`
                              <glytoucan-image imageFile="${this.imageFile}" accession="${gtc_id}" imageApi=${this.imageApi} host="${this.host}" size="100" sequence="1"></glytoucan-image>
                            `:``}
                          `)}
                        </div>
                      </td>
                    </tr>
                  `)}
                </tbody>
              </table>
            </div>
          </div>
        `;
      }
    }
  }
}

customElements.define("glycosmos-sequence", GlycosmosSequence);
