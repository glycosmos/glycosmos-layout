import { LitElement, html } from "lit-element";

class GlycosmosSummary extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      uniprotId: { type: String },
      imageFile: { type: String },
      imageApi: { type: String },
      host: { type: String },
      myArray: { type: Array, reflect: true },
      myObject: { type: Object, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "uniprot";
    this.uniprotId = "P02873";
    this.imageFile = "https://image.beta.glycosmos.org/";
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";
    this.host = "";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    let url = this.domain + "glycosmos_uniprot_summary?id=" + this.uniprotId;
    if (this.source == "gpdbs") {
      url = this.domain + "glycosmos_gpdbs_summary?id=" + this.uniprotId;
    } else if (this.source == "fgdbs") {
      url = this.domain + "glycosmos_fgdbs_summary?id=" + this.uniprotId;
    } else if (this.source == "ggdbs") {
      url = this.domain + "glycosmos_ggdbs_summary?id=" + this.uniprotId;
    } else if (this.source == "kegg") {
      url = this.domain + "glycosmos_kegg_summary?id=" + this.uniprotId;
    } else if (this.source == "gdgdbs") {
      url = this.domain + "glycosmos_gdgdbs_summary?id=" + this.uniprotId;
    }
    if (this.source == "glycans") {
      url = this.domain + "glycosmos_glycans_summary?id=" + this.uniprotId;
    }
    if (this.source == "glycolipids") {
      url = this.domain + "glycosmos_glycolipids_summary?id=" + this.uniprotId;
    }
    if (this.source == "organisms") {
      url = this.domain + "glycosmos_organisms_summary?id=" + this.uniprotId;
    }
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
    this.myObject = ["Loading..."];
    if (this.source == "uniprot" || this.source == "gpdbs" || this.source == "fgdbs") {
      fetch(this.domain + "glycosmos_external_links?id=" + this.uniprotId).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.myObject = myJson;
      });
    } else if (this.source == "glycolipids"){
      fetch(this.domain + "glycosmos_glycolipids_external_links?id=" + this.uniprotId).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.myObject = myJson;
      });
    } else {
      this.myObject = {};
    }
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Summary</div>
        <div class="card-body">
          ${this.cbodyTemplate}
          ${(this.source == "uniprot" || this.source == "gpdbs" || this.source == "glycolipids")? html`
            ${this.externalLinks}
          `:``}
        </div>
      </div>
    `;
  }

  get cbodyTemplate() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          ${(this.source == "glycans")? html`
            <div class="row">
              <div class="col-xl-6">
                <glytoucan-image imageFile="${this.imageFile}" accession="${this.uniprotId}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
              </div>
              <div class="col-xl-6">
                <div class="text-center">
                  <div class="iframe_wrapper">
                    <iframe src="https://glyconavi.org/Glycans/3d.php?gtc=${this.uniprotId}" frameborder="0"></iframe>
                  </div>
                  <a href="https://glyconavi.org/Glycans/3d.php?gtc=${this.uniprotId}" class="btn btn-primary btn-sm" target="_blank">Open LiteMol Viewer</a>
                </div>
              </div>
            </div>
          `:html`
            ${(this.source == "fgdbs")? html`
              <h5 class="card-title">${this.myArray[0]['GeneName']}</h5>
            `:html`
              ${(this.source == "ggdbs" || this.source == "kegg" || this.source == "gdgdbs")? html`
                <h5 class="card-title">${this.myArray[0]['gene_symbol']}</h5>
              `:html`
                <h5 class="card-title">${this.myArray[0]['protein_name']}</h5>
              `}
            `}
          `}
          <ul class="list-unstyled">
            ${(this.source == "glycans")? html`
              <li><strong>IUPAC</strong>: ${this.myArray[0]['iupac']}</li>
              ${this.myArray[0]['taxon'].length? html`
                <li>
                  <strong>Species</strong>:
                  <ul>
                    ${this.myArray[0]['taxon'].map(row => html`
                      <li>${row}</li>
                    `)}
                  </ul>
                </li>
            `:``}
              <li><strong>Monoisotopic mass</strong>: ${this.myArray[0]['mass']}</li>
              ${this.myArray[0]['motif'].length? html`
                <li>
                  <strong>Motif Name</strong>:
                  <ul>
                    ${this.myArray[0]['motif'].map(row => html`
                      <li>${row}</li>
                    `)}
                  </ul>
                </li>
              `:``}
              <li><strong>Subsumption</strong>: <a href="/search/glycans/mass?dtype=true&gtcid=${this.uniprotId}" target="_blank">${this.myArray[0]['subsumption']}</a></li>
              <li><a href="https://gnome.glyomics.org/StructureBrowser.html?focus=${this.uniprotId}" target="_blank"><strong>GNOme Browser</strong></li>
            `:html`
              ${(this.source == "glycolipids")? html`
                <li><strong>LM ID</strong>: <a href="https://www.lipidmaps.org/data/LMSDRecord.php?LMID=${this.myArray[0]['lm_id']}" target="_blank">${this.myArray[0]['lm_id']}</a></li>
                <li><strong>Category</strong>: ${this.myArray[0]['category']}</li>
                <li><strong>Main Class</strong>: ${this.myArray[0]['main']}</li>
                <li><strong>Sub Class</strong>: ${this.myArray[0]['sub']}</li>
                <li><strong>Exact Mass</strong>: ${this.myArray[0]['exact_mass']}</li>
                <li><strong>Formula</strong>: ${this.myArray[0]['formula']}</li>
                `:html`
                ${(this.source == "organisms")? html`
                  <li><strong>Taxonomy ID</strong>: <a href="https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=${this.myArray[0]['id']}" target="_blank">${this.myArray[0]['id']}</a></li>
                  <li><strong>Common Name</strong>: ${this.myArray[0]['commonName']}</li>
                  `:html`
                  ${(this.source == "fgdbs")? html`
                    <li><strong>CGnumber</strong>: ${this.myArray[0]['CGnumber']}</li>
                    <li><strong>FlyBase ID</strong>: <a href="${this.myArray[0]['FlyBaseLink']}" target="_blank">${this.myArray[0]['FlyBaseID']}</a></li>
                    <li><strong>NCBI ID</strong>: <a href="${this.myArray[0]['NcbiLink']}" target="_blank">${this.myArray[0]['NcbiID']}</a></li>
                    <li><strong>Mutant Stock in FlyBase</strong>: ${this.myArray[0]['MutantStock']}</li>
                    <ul>
                      <li><strong>Mutant Name</strong>: ${this.myArray[0]['MutantName']}</li>
                    </ul>
                    <li><strong>Protein Name</strong>: ${this.myArray[0]['ProteinNames']}</li>
                    <ul>
                      <li><strong>Protein Family</strong>: ${this.myArray[0]['ProteinClassNames']}</li>
                    </ul>
                    <li><strong>Mammalian Orthologue</strong>: ${this.myArray[0]['OrthologueNames']}</li>
                    <li><strong>Activity Name</strong>: ${this.myArray[0]['ActivityName']}</li>
                    <li><strong>Substrate</strong>: ${this.myArray[0]['SubstrateName']}</li>
                    <li><strong>Whole-body gene silencing Phenotype</strong>: ${this.myArray[0]['RNAiPhenoValue']}</li>
                  `:html`
                    ${(this.source == "gpdbs")? html`
                      <li><strong>GlycoProtDB</strong>: <a href="https://acgg.asia/db/gpdb/${this.myArray[0]['gpdb_id']}" target="_blank">${this.myArray[0]['gpdb_id']}</a></li>
                    `:``}
                    ${(this.source == "uniprot" || this.source == "gpdbs" || this.source == "fgdbs")? html`
                      <li><strong>UniProt ID</strong>: <a href="https://www.uniprot.org/uniprot/${this.uniprotId}" target="_blank">${this.uniprotId}</a></li>
                    `:``}
                    ${(this.source == "ggdbs" || this.source == "kegg" || this.source == "gdgdbs")? html`
                      <li><strong>Gene ID</strong>: <a href="https://www.ncbi.nlm.nih.gov/gene/${this.uniprotId}" target="_blank">${this.uniprotId}</a></li>
                    `:``}
                    <li><strong>Gene Symbol</strong>:
                      ${(this.source == "kegg")? html`
                        ${this.myArray[0]['gene_symbol_all']}
                      `:html`
                        ${this.myArray[0]['gene_symbol']}
                      `}
                    </li>
                    <li><strong>Organism</strong>:
                      <ul>
                        ${(this.source == "uniprot" || this.source == "gpdbs" || this.source == "fgdbs")? html`
                          <li><strong>Scientific Name</strong>: ${this.myArray[0]['source_name']}</li>
                        `:``}
                        ${(this.source == "ggdbs" || this.source == "kegg" || this.source == "gdgdbs")? html`
                          <li><strong>Scientific Name</strong>: ${this.myArray[0]['organism']}</li>
                        `:``}
                      </ul>
                    </li>
                    ${(this.source == "ggdbs")? html`
                      ${this.myArray[0]['description']? html`
                        <li><strong>Designation</strong>: ${this.myArray[0]['description']}</li>
                      `:``}
                      <li><strong>HGNC</strong>: <a href="${this.myArray[0]['hgnc_link']}" target="_blank">${this.myArray[0]['hgnc']}</a></li>
                      <li><strong>mRNA</strong>: <a href="${this.myArray[0]['mrna_link']}" target="_blank">${this.myArray[0]['mrna']}</a></li>
                      ${this.myArray[0]['map']? html`
                        <li><strong>map</strong>: ${this.myArray[0]['map']}</li>
                      `:``}
                      <li><strong>Protein</strong>: <a href="${this.myArray[0]['protein_link']}" target="_blank">${this.myArray[0]['protein']}</a></li>
                      ${this.myArray[0]['ec'].length? html`
                        <li>
                          <strong>EC</strong>:
                          <ul>
                            ${this.myArray[0]['ec'].map(row => html`
                              <li><a href="https://www.ebi.ac.uk/intenz/query?cmd=SearchEC&ec=${row}" target="_blank">${row}</a></li>
                            `)}
                          </ul>
                        </li>
                      `:``}
                      ${this.myArray[0]['cazy'].length? html`
                        <li>
                          <strong>CAZy</strong>:
                          <ul>
                            ${this.myArray[0]['cazy'].map(row => html`
                              <li><a href="http://www.cazy.org/GT${row}.html" target="_blank">GT${row}</a></li>
                            `)}
                          </ul>
                        </li>
                      `:``}
                      <li><strong>OMIM</strong>: <a href="https://omim.org/entry/${this.myArray[0]['omim']}" target="_blank">${this.myArray[0]['omim']}</a></li>
                    `:``}
                    ${(this.source == "ggdbs" || this.source == "kegg" || this.source == "gdgdbs")? html`
                      <li><strong>PubChem</strong>: <a href="${this.myArray[0]['pubchem_link']}" target="_blank">${this.myArray[0]['pubchem']}</a></li>
                    `:``}
                    ${(this.source == "lectins")? html`
                      <li><strong>No. of Glycosylation Sites</strong>: ${this.myArray[0]['glycosylation_sites']}</li>
                      <li><strong>Status</strong>: ${this.myArray[0]['reviewed']}</li>
                    `:``}
                  `}
                `}
              `}
            `}
          </ul>
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }

  get externalLinks() {
    if (Object.keys(this.myObject).length) {
      if (this.myObject[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">External Links</div>
            <div class="card-body">
              <ul style="padding-inline-start:20px;">
              ${(this.source == "glycolipids")? html`
                ${this.myObject[0]['lipidbank'] != "-"? html`
                  <li>Lipid Bank : <a href="http://www.lipidbank.jp/cgi-bin/detail.cgi?id=${this.myObject[0]['lipidbank']}" target="_blank">${this.myObject[0]['lipidbank']}</a></li>
                `:``}
                ${this.myObject[0]['kegg'] != "-"? html`
                  <li>KEGG : <a href="https://www.genome.jp/dbget-bin/www_bget?compound+${this.myObject[0]['kegg']}" target="_blank">${this.myObject[0]['kegg']}</a></li>
                `:``}
                ${this.myObject[0]['pubchem'] != "-"? html`
                  <li>PubChem : <a href="https://pubchem.ncbi.nlm.nih.gov/compound/${this.myObject[0]['pubchem']}" target="_blank">${this.myObject[0]['pubchem']}</a></li>
                `:``}
                ${this.myObject[0]['chebi'] != "-"? html`
                  <li>CHEBI : <a href="https://www.ebi.ac.uk/chebi/searchId.do?chebiId=CHEBI:${this.myObject[0]['chebi']}" target="_blank">${this.myObject[0]['chebi']}</a></li>
                `:``}
              `:html`
              ${(this.source == "uniprot" || this.source == "gpdbs")? html`
                ${Object.keys(this.myObject).indexOf('pubchem') !== -1? html`
                  ${this.myObject['pubchem']['uniprot_id']? html`
                    <li>PubChem : <a href="https://pubchem.ncbi.nlm.nih.gov/protein/${this.uniprotId}" target="_blank">${this.uniprotId}</a></li>
                  `:``}
                `:``}
                ${Object.keys(this.myObject).indexOf('gpdb') !== -1? html`
                  ${this.myObject['gpdb']['gpdb_id']? html`
                    <li>GlycoProtDB : <a href="https://acgg.asia/gpdb2/${this.myObject['gpdb']['gpdb_id']}" target="_blank">${this.myObject['gpdb']['gpdb_id']}</a></li>
                  `:``}
                `:``}
                ${Object.keys(this.myObject).indexOf('glyconnect') !== -1? html`
                  ${this.myObject['glyconnect']['glyconnect_id'] != ""? html`
                    <li>GlyConnect :
                      ${this.myObject['glyconnect']['glyconnect_id'].map(row => html`
                        <a href="https://glyconnect.expasy.org/browser/proteins/${row}" target="_blank">${row}</a>
                      `)}
                    </li>
                  `:``}
                `:``}
                ${Object.keys(this.myObject).indexOf('glygen') !== -1? html`
                  ${Object.keys(this.myObject['glygen']).indexOf('uniprot_id') !== -1? html`
                    ${this.myObject['glygen']['uniprot_id']? html`
                      <li>GlyGen : <a href="http://glygen.org/protein/${this.myObject['glygen']['uniprot_id']}" target="_blank">${this.myObject['glygen']['uniprot_id']}</a></li>
                    `:``}
                  `:``}
                  ${Object.keys(this.myObject['glygen']).indexOf('unicarbkb') !== -1 && Object.keys(this.myObject['glygen']).indexOf('unicarbkb') !== -1? html`
                    ${this.myObject['glygen']['unicarbkb']? html`
                      <li>UniCarbKB : ${this.myObject['glygen']['uniprot_id']}</li>
                    `:``}
                  `:``}
                `:``}
              `:``}
              `}
              </ul>
            </div>
          </div>
        `;
      }
    } else {
      return html`
      <div class="card mt-3">
        <div class="card-header">External Links</div>
        <div class="card-body">
          <div class="py-3">Data not found</div>
        </div>
      </div>
      `;
    }
  }
}

customElements.define("glycosmos-summary", GlycosmosSummary);
