import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosLfdb extends LitElement {
  static get properties() {
    return {
      myArray: { type: Array, reflect: true },
      domain: { type: String },
      variable: { type: String },
      host: { type: String },
      imageApi: { type: String },
      imageFile: { type: String }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.variable = "P07583";
    this.host = "";
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";
    this.imageFile = "https://image.beta.glycosmos.org/";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    let url = this.domain + "glycosmos_lectins_lfdb?uniprot=" + this.variable;
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">LfDB</div>
        <div class="card-body">
          <p>Top 3 high affinity glycans and the actual measurement (V-V<sub>0</sub>) values by an automated frontal affinity chromatography with fluorescence detection from LfDB.</p>
          ${this.lfdb}
        </div>
      </div>
    `;
    }

  get lfdb() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card">
            <div class="card-body">
              <div class="mb-4"><strong><p>LfDB:<a href="https://acgg.asia/lfdb2/${this.myArray[0]['lfdbid']}" target="_blank">${this.myArray[0]['lfdbid']}</a></p></strong></div>
              <div class="container mt-3">
                <div class="row row-cols-2 row-cols-md-3 row-cols-xl-4">
                  ${this.myArray.map((row) => html`
                    <div class="col col-xl-3 mb-4">
                      <div class="card h-100">
                        <div class="card-body">
                          <glytoucan-image imageFile="${this.imageFile}" accession="${row['glytoucan_id']}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
                        </div>
                        <div class="card-footer">V-V<sub>0</sub> : ${row['value']}</div>
                      </div>
                    </div>
                  `)}
                </div>
              </div>
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }
}

customElements.define("glycosmos-lfdb", GlycosmosLfdb);
