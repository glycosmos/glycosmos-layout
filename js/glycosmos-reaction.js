import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosReaction extends LitElement {
  static get properties() {
    return {
      listSparqlet: { type: String },
      addSparqlet: { type: String },
      variable: { type: String },
      host: { type: String },
      title: { type: String },
      discription: { type: String },
      arrow: { type: String },
      imageApi: { type: String },
      imageFile: { type: String },
      myObject: { type: Object, reflect: true },
      addReferences: { type: Object, reflect: true }
    };
  }

  constructor() {
    super();
    this.listSparqlet = "https://test.sparqlist.glycosmos.org/sparqlist/api/glycosmos_ggdbs_reaction?id=";
    this.addSparqlet = "";
    this.variable = "28";
    this.host = "";
    this.title = "General Reaction";
    this.discription = '';
    this.arrow = '';
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";
    this.imageFile = "https://image.beta.glycosmos.org/";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myObject = {"status": "Loading..."};
    fetch(this.listSparqlet + this.variable).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
    });
    this.addReferences = {};
    if (this.addSparqlet) {
      fetch(this.addSparqlet + this.variable).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.addReferences = myJson;
      });
    }
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">${this.title}</div>
        <div class="card-body">
          ${this.discription? html`
            ${unsafeHTML(this.discription)}
          `:``}
          ${this.cardTemplate}
        </div>
      </div>
    `;
  }

  get cardTemplate() {
    if (Object.keys(this.myObject).length) {
      if (this.myObject["status"] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          ${Object.keys(this.myObject).map(title => html`
            <div class="card mt-4">
              ${title != "undefined"? html`
                <div class="card-header text-muted">Reference: <a href="${this.myObject[title][0].pub_link}" target="_blank">${title}</a></div>
              `:``}
              ${this.myObject[title].length? html`
                <div class="card-body">
                  <div class="container mt-4">
                    <div class="row row-cols-1 row-cols-xl-2">
                      ${this.myObject[title].map(row => html`
                        <div class="col col-xl-6 mb-4">
                          ${Object.keys(row).indexOf('acceptor_gtc_id') !== -1? html`
                            <div class="card h-100">
                              <div class="card-body">
                                ${row.reaction_type == "Substrate_not_transferred"? html`
                                  <h6 class="card-subtitle text-muted mb-3">Substrate Not Transferred</h6>
                                `:``}
                                <h6 class="card-subtitle mb-2">Donor: ${row.donor_id}</h6>
                                <table class="table table-borderless">
                                  <tbody>
                                    <tr>
                                      <td class="align-middle">
                                        ${row.product_gtc_id? html`
                                          <div class="float-right"><glytoucan-image imageFile="${this.imageFile}" accession="${row.acceptor_gtc_id}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image></div>
                                        `:html`
                                          <div class="float-left"><glytoucan-image imageFile="${this.imageFile}" accession="${row.acceptor_gtc_id}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image></div>
                                        `}
                                      </td>
                                      ${row.product_gtc_id? html`
                                        <td class="align-middle">
                                          ${this.arrow? html`
                                            ${unsafeHTML(this.arrow)}
                                          `:html`
                                            <h2 class="text-center">></h2>
                                          `}
                                        </td>
                                        <td class="align-middle">
                                          <div class="float-left"><glytoucan-image imageFile="${this.imageFile}" accession="${row.product_gtc_id}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image></div>
                                        </td>
                                      `:``}
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          `:``}
                        </div>
                      `)}
                    </div>
                  </div>
                </div>
              `:``}
            </div>
          `)}
          ${Object.keys(this.addReferences).length? html`
            ${Object.keys(this.addReferences).map(add => html`
              ${Object.keys(this.myObject).indexOf(add) == -1? html`
                <div class="card mt-4">
                  <div class="card-header text-muted">Reference: <a href="${this.addReferences[add][0].pub_link}" target="_blank">${add}</a></div>
                </div>
              `:``}
            `)}
          `:``}
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }
}

customElements.define("glycosmos-reaction", GlycosmosReaction);
