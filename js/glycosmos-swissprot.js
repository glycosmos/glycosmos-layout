import { LitElement, html } from "lit-element";

class GlycosmosSwissprot extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      cgNumber: { type: String },
      myArray: { type: Array, reflect: true },
      fasta: { type: String, reflect: true },
      mySequence: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.cgNumber = "CG10000";
  }

  connectedCallback() {
    super.connectedCallback();
    this.fasta = "";
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    this.mySequence = ["Loading..."];
    fetch(this.domain + "glycosmos_fgdbs_swissprot?id=" + this.cgNumber).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
      if (this.myArray.length) {
        this.getSequence(this.myArray[0].Uniprot_label[0]);
      } else {
        this.mySequence = "";
      }
    });
  }

  getSequence(uniprotId) {
    this.fasta = "https://www.uniprot.org/uniprot/" + uniprotId + ".fasta";
    fetch(this.fasta).then((response) => {
      return response.text();
    }).then((myJson) => {
      let text = myJson.split('\n');
      let seq = text.slice(1).join('');
      let lines = [];
      for (let i = 0; i < seq.length; i += 50) {
        lines.push(seq.substring(i, i + 50));
      }
      let array = lines.map((line, index) => 
        ("        " + String((index * 5 + 1) * 10)).substr(-10) + " " +
        ("        " + String((index * 5 + 2) * 10)).substr(-10) + " " +
        ("        " + String((index * 5 + 3) * 10)).substr(-10) + " " +
        ("        " + String((index * 5 + 4) * 10)).substr(-10) + " " +
        ("        " + String((index * 5 + 5) * 10)).substr(-10) + "\n" + 
        line.slice(0, 10) + " " + line.slice(10, 20) + " " + line.slice(20, 30) + " " + line.slice(30, 40) + " " + line.slice(40, 50) + "\n"
      );
      this.mySequence = array.join('\n');
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Swiss-Prot Entry</div>
        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Uniprot ID</th>
                <th scope="col">KEGG ID</th>
                <th scope="col">CAZy ID</th>
              </tr>
            </thead>
            <tbody>
              ${this.tbodyTemplate}
            </tbody>
          </table>
          ${this.mySequence.length? html`
            <div class="card mt-3">
              <div class="card-header">Sequence</div>
              <div class="card-body">
                ${this.aminoAcidSequence}
              </div>
              ${this.fasta? html`
                <div class="card-footer text-muted">
                  <a href="${this.fasta}" target="_blank">FASTA</a>
                </div>
              `:``}
            </div>
          `:``}
        </div>
      </div>
    `;
  }

  get tbodyTemplate() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <tr>
            <td colspan="3">
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            </td>
          </tr>
        `;
      } else {
        return html`
          <tr>
            <td scope="row">
              <ul style="padding-inline-start:20px;">
                ${this.myArray[0].Uniprot_label.map(uniprotId => html`
                  ${uniprotId == "No data"? html`
                    ${uniprotId}
                  `:html`
                    <li><a href="http://purl.uniprot.org/uniprot/${uniprotId}" target="_blank">${uniprotId}</a></li>
                  `}
                `)}
              </ul>
            </td>
            <td scope="row">
              <ul style="padding-inline-start:20px;">
                ${this.myArray[0].KEGG_label.map(keggId => html`
                  ${keggId == "No data"? html`
                    ${keggId}
                  `:html`
                    <li><a href="http://purl.uniprot.org/kegg/dme:Dmel_${keggId}" target="_blank">${keggId}</a></li>
                  `}
                `)}
              </ul>
            </td>
            <td scope="row">
              <ul style="padding-inline-start:20px;">
                ${this.myArray[0].CAZy_label.map(cazyId => html`
                  ${cazyId == "No data"? html`
                    ${cazyId}
                  `:html`
                    <li><a href="http://purl.uniprot.org/cazy/${cazyId}" target="_blank">${cazyId}</a></li>
                  `}
                `)}
              </ul>
            </td>
          </tr>
        `;
      }
    } else {
      return html`
        <tr>
          <td colspan="3" class="text-center">
            <div class="py-3">Data not found</div>
          </td>
        </tr>
      `;
    }
  }

  get aminoAcidSequence() {
    if (this.mySequence[0] == "Loading...") {
      return html`
        <div class="d-flex justify-content-center">
          <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      `;
    } else {
      return html`
        <pre>${this.mySequence}</pre>
      `;
    }
  }
}

customElements.define("glycosmos-swissprot", GlycosmosSwissprot);
