import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosAnnotation extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      uniprotId: { type: String },
      keyArray: { type: Array, reflect: true },
      siteArray: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "uniprot";
    this.uniprotId = "P02873";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.keyArray = ["Loading..."];
    let key = this.domain + "glycosmos_uniprot_keywords?uniprot=" + this.uniprotId;
    fetch(key).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.keyArray = myJson;
    });
    this.siteArray = ["Loading..."];
    let positionUrl = this.domain + "glycosmos_uniprot_site?uniprot=" + this.uniprotId;
    fetch(positionUrl).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.siteArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Annotation</div>
        <div class="card-body">
        <p>Annotation information from <a href="https://www.uniprot.org/" target="_blank">UniProt</a></p>
        <div class="row">
          <div class="col-lg-4">
            ${this.key}
          </div>
          <div class="col-lg-8">
            ${this.sites}
          </div>
        </div>
      </div>
    `;
  }

  get key() {
    if (this.keyArray.length) {
      if (this.keyArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">Keywords</div>
            <div class="card-body">
              ${this.keyArray.map((row) => html`
                  <li>${row['keyword']}</li>
              `)}
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="card mt-3">
          <div class="card-header">Keywords</div>
          <div class="card-body">
            <div class="py-3">Data not found</div>
          </div>
        </div>
      `;
    }
  }

  get sites() {
    if (this.siteArray.length) {
      if (this.siteArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">Sites</div>
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Position</th>
                    <th scope="col">Description</th>
                  </tr>
                </thead>
                <tbody>
                  ${this.siteArray.map((row, index) => html`
                    <tr>
                      <th scope="row">${index + 1}</th>
                      <td>${row['Position']}</td>
                      <td>${row['Description']}</td>
                    </tr>
                  `)}
                </tbody>
              </table>
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="card mt-3">
          <div class="card-header">Sites</div>
          <div class="card-body">
            <div class="py-3">Data not found</div>
          </div>
        </div>
      `;
    }
  }
}

customElements.define("glycosmos-annotation", GlycosmosAnnotation);
