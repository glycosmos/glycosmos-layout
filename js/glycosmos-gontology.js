import { LitElement, html } from "lit-element";

class GlycosmosGontology extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      gene_id: { type: String },
      cgNumber: { type: String },
      myObject: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.gene_id = "10678";
    this.cgNumber = "";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myObject = ["Loading..."];
    let url = this.domain + "glycosmos_ggdbs_gontology?id=" + this.gene_id;
    if (this.cgNumber) {
      url = this.domain + "glycosmos_fgdbs_gontology?id=" + this.cgNumber;
    }
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Gene Ontology</div>
        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Biological Process</th>
                <th scope="col">Cellular Component</th>
                <th scope="col">Molecular Function</th>
              </tr>
            </thead>
            <tbody>
              ${this.tbodyTemplate}
            </tbody>
          </table>
        </div>
      </div>
    `;
  }

  get tbodyTemplate() {
    if (this.myObject[0] == "Loading...") {
      return html`
        <tr>
          <td colspan="3">
            <div class="d-flex justify-content-center">
              <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>
          </td>
        </tr>
      `;
    } else {
      return html`
        ${this.myObject.biological_process.length || this.myObject.cellular_component.length || this.myObject.molecular_function.length? html`
          <tr>
            <td scope="row">
              <ul style="padding-inline-start:20px;">
                ${this.cgNumber? html`
                  ${this.myObject.biological_process.map(row => html`
                    <li><a href="${row.go_keyword}" target="_blank">${row.go_label}</a></li>
                  `)}
                `:html`
                  ${this.myObject.biological_process.map(row => html`
                    <li><a href="http://amigo.geneontology.org/amigo/term/${row.go_id}" target="_blank">${row.go_label}</a></li>
                  `)}
                `}
              </ul>
            </td>
            <td scope="row">
              <ul style="padding-inline-start:20px;">
                ${this.cgNumber? html`
                  ${this.myObject.cellular_component.map(row => html`
                    <li><a href="${row.go_keyword}" target="_blank">${row.go_label}</a></li>
                  `)}
                `:html`
                  ${this.myObject.cellular_component.map(row => html`
                    <li><a href="http://amigo.geneontology.org/amigo/term/${row.go_id}" target="_blank">${row.go_label}</a></li>
                  `)}
                `}
              </ul>
            </td>
            <td scope="row">
              <ul style="padding-inline-start:20px;">
                ${this.cgNumber? html`
                  ${this.myObject.molecular_function.map(row => html`
                    <li><a href="${row.go_keyword}" target="_blank">${row.go_label}</a></li>
                  `)}
                `:html`
                  ${this.myObject.molecular_function.map(row => html`
                    <li><a href="http://amigo.geneontology.org/amigo/term/${row.go_id}" target="_blank">${row.go_label}</a></li>
                  `)}
                `}
              </ul>
            </td>
          </tr>
        `:html`
          <tr>
            <td colspan="3" class="text-center">
              <div class="py-3">Data not found</div>
            </td>
          </tr>
        `}
      `;
    }
  }
}

customElements.define("glycosmos-gontology", GlycosmosGontology);
