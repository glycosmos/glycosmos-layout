import { LitElement, html } from "lit-element";

class GlycosmosExternal extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      title: { type: String },
      key: { type: String },
      myObject: { type: Object, reflect: true },
      dbArray: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "glycans";
    this.title = "External Links";
    this.key = "G00052MO";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myObject = ["Loading..."];
    if(this.source == "glycans"){
    let url = this.domain + "glycosmos_glycans_external_links?accNum=" + this.key;
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
      this.dbArray = Object.keys(this.myObject);
    });
  }else if (this.source == "disease") {
    let url = this.domain + "glycosmos_glycogenes_disease?id=" + this.key;
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
      this.dbArray = Object.keys(this.myObject);
    });
  }
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">${this.title}</div>
        <div class="card-body">
          ${this.cbodyTemplate}
        </div>
      </div>
    `;
  }

  get cbodyTemplate() {
    if (Object.keys(this.myObject).length) {
      if (this.myObject == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
            <div class="card-body">
              <ul style="padding-inline-start:20px;">
              ${this.dbArray.map((db) => html`
                ${Object.keys(this.myObject).indexOf(db) !== -1? html`
                  <li>${db} :
                  ${(this.source == "glycans")? html`
                    <br>
                  `:html`
                  `}
                    ${this.myObject[db]['list'].map(row => html`
                      <a href="${row['url']}" target="_blank">${row['id']}&nbsp;&nbsp;</a>
                    `)}
                  </li>
                `:``}
                `)}
              </ul>
            </div>
        `;
      }
    } else {
      return html`
      <div class="py-3">Data not found</div>
      `;
    }
  }

}

customElements.define("glycosmos-external", GlycosmosExternal);
