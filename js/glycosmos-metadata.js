import { LitElement, html } from "lit-element";

class GlycosmosMetadata extends LitElement {
  static get properties() {
    return {
      wrapper: { type: String },
      myObject: { type: Object, reflect: true },
      glyconavidate: { type: String },
      glycandate: { type: String }
    };
  }

  constructor() {
    super();
    this.wrapper = "";
    this.categories = ["Gene", "Protein", "Lipids", "Glycans", "Glycoconjugates", "Glycomes", "Pathways", "Diseases"];
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myObject = {"status": "Loading..."};
    const spreadsheet = "AKfycbxYo1CwXiJGTqyGsg7Nh7XteEDfu0xacncIRuUp1fNXUh8wahI";
    const url = this.wrapper + "https://script.google.com/macros/s/" + spreadsheet + "/exec";
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
    });
    this.glyconavidate = "Loading...";
    const g_url = "https://sparqlist.glyconavi.org/api/TCarp_LastUpdate_text";
    fetch(g_url).then((response) => {
      return response.text();
    }).then((myJson) => {
      this.glyconavidate = myJson;
    });
    this.glycandate = "Loading...";
    const glycan_url = "https://glycosmos.org/wrappers/lastup.json?url=http://glycosmos-sparqlist:3000/sparqlist/api/glycosmos_glycans_list";
    fetch(glycan_url).then((response) => {
      return response.text();
    }).then((myJson) => {
      this.glycandate = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="container-fluid">
        <table class="table table-bordered table-hover table-sm" style="border-collapse: separate; border-spacing: 0;">
          <thead>
            <tr>
              <th scope="col">Category</th>
              <th scope="col">Subcategory</th>
              <th scope="col">Database</th>
              <th scope="col">Last Update</th>
              <th scope="col">Source</th>
            </tr>
          </thead>
          <tbody>
            ${this.tbodyTemplate}
          </tbody>
        </table>
      </div>
    `;
  }

  get tbodyTemplate() {
    if (this.myObject["status"] == "Loading...") {
      return html`
        <tr>
          <td colspan="${this.categories.length}">
            <div class="d-flex justify-content-center">
              <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                <span class="sr-only">Loading...</span>
              </div>
            </div>
          </td>
        </tr>
      `;
    } else {
      let beforeCategory = "";
      let beforeSubcategory = "";
      let array = [];
      return html`
        ${this.categories.map(category => html`
          ${Object.keys(this.myObject[category]).map(subcategory => html`
            ${Object.keys(this.myObject[category][subcategory]).map(database => html`
              <tr>
                ${category != beforeCategory? html`
                  <th class="align-middle" rowspan="${Object.keys(this.myObject[category]).map(row => Object.keys(this.myObject[category][row])).flat().length}">${category}</th>
                  ${console.log(beforeCategory = category)}
                `:html``}
                ${subcategory != beforeSubcategory? html`
                  <th class="align-middle" rowspan="${Object.keys(this.myObject[category][subcategory]).length}">${subcategory}</th>
                  ${console.log(beforeSubcategory = subcategory)}
                `:html``}
                <th class="align-middle">${database}</th>
                ${database.indexOf('GlycoNAVI') != -1 && database != "GlycoNAVI-Genes" && database != "GlycoNAVI-Diseases"? html`
                  <th class="align-middle">${this.glyconavidate}</th>
                `:html`
                  ${database == "GlyTouCan"? html`
                    <th class="align-middle">${this.glycandate}</th>
                  `:html`
                    <th class="align-middle">${this.myObject[category][subcategory][database]["Update"]}</th>
                    `}
                  `}
                ${this.myObject[category][subcategory][database]["Source"].startsWith("http")? html`
                  <th class="align-middle"><a href="${this.myObject[category][subcategory][database]["Source"]}" target="_blank">${this.myObject[category][subcategory][database]["Source"]}</a></th>
                `:html`
                  <th class="align-middle">${this.myObject[category][subcategory][database]["Source"]}</th>
                `}
              </tr>
            `)}
          `)}
        `)}
      `;
    }
  }
}

customElements.define("glycosmos-metadata", GlycosmosMetadata);
