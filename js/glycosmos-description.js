import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosDescription extends LitElement {
  static get properties() {
    return {
      wrapper: { type: String },
      domain: { type: String },
      page: { type: String },
      section: { type: String },
      key: { type: String },
      databases: { type: Object },
      header: { type: Array },
      myObject: { type: Object, reflect: true },
      glyconavidate: { type: String },
      glycandate: { type: String },
      metaData: { type: Object, reflect: true }
    };
  }

  constructor() {
    super();
    this.wrapper = "";
    this.domain = "https://glycosmos.org/";
    this.page = "glycogenes";
    this.section = "index";
    this.key = "description";
    this.databases = {};
    this.header = ["Database Name", "Last Update"];
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
    this.getData();
  }

  getJson() {
    this.myObject = {"status": "Loading..."};
    const spreadsheet = "AKfycbwkkK9rPrtvpX-1ojDe1ijl6sQR-k0LHL6dHnkb0XG_Fop6WXY";
    const url = this.wrapper + "https://script.google.com/macros/s/" + spreadsheet + "/exec";
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
    });
    this.glyconavidate = "Loading...";
    const g_url = "https://sparqlist.glyconavi.org/api/TCarp_LastUpdate_text";
    fetch(g_url).then((response) => {
      return response.text();
    }).then((myJson) => {
      this.glyconavidate = myJson;
    });
    this.glycandate = "Loading...";
    const glycan_url = this.domain + "wrappers/lastup.json?url=http://glycosmos-sparqlist:3000/sparqlist/api/glycosmos_glycans_list";
    fetch(glycan_url).then((response) => {
      return response.text();
    }).then((myJson) => {
      this.glycandate = myJson;
    });
  }

  getData() {
    this.metaData = {"status": "Loading..."};
    const spreadsheet = "AKfycbxYo1CwXiJGTqyGsg7Nh7XteEDfu0xacncIRuUp1fNXUh8wahI";
    const url = this.wrapper + "https://script.google.com/macros/s/" + spreadsheet + "/exec";
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.metaData = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      ${Object.keys(this.databases).length? html`
        <div class="row">
          <div class="col-lg-6">
            <p>${this.textTemplate}</p>
          </div>
          <div class="col-lg-6">
            <p>${this.tableTemplate}</p>
          </div>
        </div>
      `:html`
        ${this.textTemplate}
      `}
    `;
  }

  get textTemplate() {
    if (this.myObject["status"] == "Loading...") {
      return html`
        <div class="d-flex align-items-center">
          <strong>Loading...</strong>
          <div class="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></div>
        </div>
      `;
    } else {
      return html`
        ${unsafeHTML(this.myObject[this.page][this.section][this.key])}
      `;
    }
  }

  get tableTemplate() {
    return html`
      <table class="table table-bordered table-hover table-sm" style="border-collapse: separate; border-spacing: 0;">
        <thead>
          <tr>
            <th scope="col">${this.header[0]}</th>
            <th scope="col">${this.header[1]}</th>
          </tr>
        </thead>
        <tbody>
          ${Object.keys(this.databases).map(category => html`
            ${Object.keys(this.databases[category]).map(subcategory => html`
              ${this.databases[category][subcategory].map(database => html`
                <tr>
                  ${this.metaData["status"] == "Loading..."? html`
                    <td colspan="2">
                      <div class="d-flex align-items-center">
                        <strong>Loading...</strong>
                        <div class="spinner-border spinner-border-sm ml-2" role="status" aria-hidden="true"></div>
                      </div>
                    </td>
                  `:html`
                    ${this.metaData[category][subcategory][database]['Source'].slice(0, 4) == "http"? html`
                      <td><a href="${this.metaData[category][subcategory][database]['Source']}" target="_blank">${database}</a></td>
                    `:html`
                      <td>${database}</td>
                    `}
                    ${database.indexOf('GlycoNAVI') != -1? html`
                      <td>${this.glyconavidate}</td>
                    `:html`
                      ${database == "GlyTouCan"? html`
                        <td>${this.glycandate}</td>
                      `:html`
                        <td>${this.metaData[category][subcategory][database]['Update']}</td>
                      `}
                    `}
                  `}
                </tr>
              `)}
            `)}
          `)}
        </tbody>
      </table>
    `;
  }
}

customElements.define("glycosmos-description", GlycosmosDescription);
