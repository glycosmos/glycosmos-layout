import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosRhea extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      variable: { type: String },
      myArray: { type: Array, reflect: true },
      arrow: { type: String }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.variable = "28";
    this.arrow = '';
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    fetch(this.domain + "glycosmos_glycogenes_reaction_rhea?id=" + this.variable).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header"></div>
        <div class="card-body">
          ${this.reaction}
        </div>
      </div>
    `;
  }

  get reaction() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          ${this.myArray.map((row) => html`
                <div class="card">
                  <div class="card-header"><a href="${row['reaction']}" target="_blank">${row['accession']}</a></div>
                  <div class="card-body">
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">Reaction</th>
                          <th scope="col">Enzyme</th>
                          <th scope="col">PMID</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td>
                          <table class="table table-borderless">
                            <tbody>
                              <tr>
                                <td class="align-middle">
                                  ${unsafeHTML(row.left.join(' <b>+</b> '))}
                                </td>
                                <td class="align-middle">
                                  ${this.arrow? html`
                                    ${unsafeHTML(this.arrow)}
                                  `:html`
                                    <h2 class="text-center">></h2>
                                  `}
                                </td>
                                <td class="align-middle">
                                  ${unsafeHTML(row.right.join(' <b>+</b> '))}
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                        <td>
                          <ul class="list-unstyled">
                            ${row.enzyme.map((enzyme) => html`
                              <li><a href="https://enzyme.expasy.org/EC/${enzyme}" target="_blank">${enzyme}</a></li>
                            `)}
                          </ul>
                        </td>
                        <td>
                          <ul class="list-unstyled">
                            ${row.pubmed.map((pubmed) => html`
                              <li><a href="https://pubmed.ncbi.nlm.nih.gov/${pubmed}" target="_blank">${pubmed}</a></li>
                            `)}
                          </ul>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
          `)}
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }
}

customElements.define("glycosmos-rhea", GlycosmosRhea);
