import { LitElement, html } from "lit-element";

class GlycosmosPathway extends LitElement {
  static get properties() {
    return {
      sparqlet: { type: String },
      variable: { type: String },
      pathwayLink: { type: String },
      myArray: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.sparqlet = "https://test.sparqlist.glycosmos.org/sparqlist/api/pathway_search_by_protein";
    this.variable = "Q96P68";
    this.pathwayLink = "https://test.glycosmos.org/pathways/show?id=";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    fetch(this.sparqlet + "?key=" + this.variable).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Pathway</div>
        <div class="card-body">
          ${this.myArray.length? html`
            ${this.myArray[0] == "Loading..."? html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `:html`
              <p>Associated pathways from <a href="https://reactome.org/PathwayBrowser/" target="_blank">Reactome</a></p>
              <ul>
                ${this.myArray.map(row => html`
                  <li><a href="${this.pathwayLink}${row.pathway_ID}&key=${this.variable}" target="_blank">${row.pathwayName}(${row.organism})</a></li>
                `)}
              </ul>
            `}
          `:html`
            <div class="py-3">Data not found</div>
          `}
        </div>
      </div>
    `;
  }
}

customElements.define("glycosmos-pathway", GlycosmosPathway);
