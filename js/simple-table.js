import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class SimpleTable extends LitElement {
  static get properties() {
    return {
      listSparqlet: { type: String },
      variable: { type: String },
      title: { type: String },
      discription: { type: String },
      columns: { type: Object },
      links: { type: Object },
      horizontal: { type: Boolean },
      myArray: { type: Array, reflect: true },
      imageFile: { type: String }
    };
  }

  constructor() {
    super();
    this.listSparqlet = "https://test.sparqlist.glycosmos.org/sparqlist/api/glycosmos_glycoproteins_gdgdb?id=";
    this.variable = "Q13510";
    this.title = "Glyco-Disease Genes Database (GDGDB)";
    this.discription = '';
    this.columns = {
      "concept_ui": {
        "name": "Concept UI"
      },
      "disease_name": {
        "name": "Disease name"
      },
      "alt_disease_names": {
        "name": "Aliases"
      },
      "disease_type": {
        "name": "Disease type"
      }
    };
    this.links = {
      "concept_ui": {
        "url": "https://acgg.asia/db/diseases/gdgdb?con_ui=",
        "key": "concept_ui"
      }
    };
    this.horizontal = false;
    this.imageFile = "https://image.beta.glycosmos.org/";
  }

  connectedCallback() {
    super.connectedCallback();
    this.columnNames = Object.keys(this.columns);
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    fetch(this.listSparqlet + this.variable).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">${this.title}</div>
        <div class="card-body">
          ${this.discription? html`
            ${unsafeHTML(this.discription)}
          `:``}
          <table class="table">
            <thead>
              ${this.theadTemplate}
            </thead>
            <tbody>
              ${this.tbodyTemplate}
            </tbody>
          </table>
        </div>
      </div>
    `;
  }

  get theadTemplate() {
    return html`
      ${!this.horizontal? html`
        <tr>
          ${this.columnNames.map(col => html`
            <th scope="col">${unsafeHTML(this.columns[col].name)}</th>
          `)}
        </tr>
      `:``}
    `;
  }

  get tbodyTemplate() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          ${this.horizontal? html`
            <tr>
              <td rowspan="2">
                <div class="d-flex justify-content-center">
                  <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                </div>
              </td>
            </tr>
          `:html`
            <tr>
              <td colspan="${this.columnNames.length}">
                <div class="d-flex justify-content-center">
                  <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                </div>
              </td>
            </tr>
          `}
        `;
      } else {
        return html`
          ${this.horizontal? html`
            ${this.myArray.map(row => html`
              ${this.columnNames.map(col => html`
                <tr>
                  <th scope="row">${unsafeHTML(this.columns[col].name)}</th>
                  <td>
                    ${this.links.hasOwnProperty(col)? html`
                      <a href="${this.links[col].url}${row[this.links[col].key]}" target="_blank">${row[col]}</a>
                    `:html`
                      ${col == "glycoct"? html`
                        <ul class="list-unstyled">
                          ${row[col].split('\n').map(row => html`
                            <li>${row}</li>
                          `)}
                        </ul>
                      `:html`
                        ${row[col]}
                      `}
                    `}
                  </td>
                </tr>
              `)}
            `)}
          `:html`
            ${this.myArray.map(row => html`
              <tr>
                ${this.columnNames.map(col => html`
                  ${col == "glycan_image"? html`
                    <td>
                      <glytoucan-image imageFile="${this.imageFile}" accession="${row["glycan"]}"></glytoucan-image>
                    </td>
                  `:html`
                    <td>
                      ${this.links.hasOwnProperty(col)? html`
                        <a href="${this.links[col].url}${row[this.links[col].key]}" target="_blank">${row[col]}</a>
                      `:html`
                      ${col == "unilectin_pdbs"? html`
                        <ul class="list-unstyled">
                          ${row[col].map(row => html`
                            <li><a href="https://www.unilectin.eu/curated/structure?id=${row}" target="_blank">${row}</a></li>
                          `)}
                        </ul>
                      `:html`
                        ${row[col]}
                      `}
                    `}
                    </td>
                  `}
                `)}
              </tr>
            `)}
          `}
        `;
      }
    } else {
      return html`
        <tr>
          <td colspan="${this.columnNames.length}" class="text-center">
            <div class="py-3">Data not found</div>
          </td>
        </tr>
      `;
    }
  }
}

customElements.define("simple-table", SimpleTable);
