import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosFaq extends LitElement {
  static get properties() {
    return {
      wrapper: { type: String },
      myObject: { type: Object, reflect: true }
    };
  }

  constructor() {
    super();
    this.wrapper = "";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myObject = {"status": "Loading..."};
    const spreadsheet = "AKfycbwSOietY2JfHbJLvvcdvXVUSjS-kPNs0-9TtsOWNI2xmiDZXQ";
    const url = this.wrapper + "https://script.google.com/macros/s/" + spreadsheet + "/exec";
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson["results"];
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="container-fluid">
        ${this.listTemplate}
      </div>
    `;
  }

  get listTemplate() {
    if (this.myObject["status"] == "Loading...") {
      return html`
        <div class="d-flex justify-content-center">
          <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
            <span class="sr-only">Loading...</span>
          </div>
        </div>
      `;
    } else {
      return html`
        <h3 class="mb-2"></h3>
        ${Object.keys(this.myObject).map(question => html`
          <ul>
            <li class="my-2">${unsafeHTML(question)}</li>
            <ul>
              <li>${unsafeHTML(this.myObject[question])}</li>
            </ul>
          </ul>
        `)}
      `;
    }
  }
}

customElements.define("glycosmos-faq", GlycosmosFaq);
