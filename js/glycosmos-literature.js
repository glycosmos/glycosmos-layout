import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosLiterature extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      key: { type: String },
      myArray: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "glycocprotein";
    this.key = "P00742";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    if(this.source == "glycan"){
      this.myArray = ["Loading..."];
      let pmid = this.domain + "glycosmos_glycan_literature?id=" + this.key;
      fetch(pmid).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.myArray = myJson;
        this.bcsdbArray = this.myArray.filter(function(item, index){
          if (item.database == 'BCSDB') return true;
        });
        this.epitopeArray = this.myArray.filter(function(item, index){
          if (item.database == 'GlycoEpitope') return true;
        });
      });
    }else if(this.source == "glycoprotein"){
      this.myArray = ["Loading..."];
      let pmid = this.domain + "glycosmos_glycoproteins_literature?id=" + this.key;
      fetch(pmid).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.myArray = myJson;
        this.glygenArray = this.myArray.filter(function(item, index){
          if (item.database == 'glygen') return true;
        });
        this.glyconnectArray = this.myArray.filter(function(item, index){
          if (item.database == 'glyconnect') return true;
        });
      });
    }
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Literature</div>
        <div class="card-body">
          <div class="row">
            <div class="col-lg-12">
              ${this.literature1}
            </div>
            <div class="col-lg-12">
              ${this.literature2}
            </div>
          </div>
        </div>
      </div>
    `;
  }

  get literature1() {
    if(this.source == "glycan"){
      if (this.bcsdbArray.length) {
      if (this.bcsdbArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">from BCSDB</div>
            <div class="card-body">
              ${this.bcsdbArray.map((row) => html`
                <ul style="display: inline-block;">
                  <li><small><a href="${row['url']}" target="_blank">${row['label']}</a></small></li>
                </ul>
              `)}
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="card mt-3">
        <div class="card-header">from BCSDB</div>
          <div class="card-body">
            <div class="py-3">Data not found</div>
          </div>
        </div>
      `;
    }
  } else if (this.source == "glycoprotein"){
    if (this.glygenArray.length) {
      if (this.glygenArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">from GlyGen</div>
            <div class="card-body">
              ${this.glygenArray.map((row) => html`
                <ul style="display: inline-block;">
                  <li><small><a href="${row['url']}" target="_blank">${row['label']}</a></small></li>
                </ul>
              `)}
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="card mt-3">
        <div class="card-header">from GlyGen</div>
          <div class="card-body">
            <div class="py-3">Data not found</div>
          </div>
        </div>
      `;
    }
  }
  }
  get literature2() {
    if(this.source == "glycan"){
      if (this.epitopeArray.length) {
      if (this.epitopeArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">from GlycoEpitope</div>
            <div class="card-body">
              ${this.epitopeArray.map((row) => html`
                <ul style="display: inline-block;">
                  <li><small><a href="${row['url']}" target="_blank">${row['label']}</a></small></li>
                </ul>
              `)}
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="card mt-3">
        <div class="card-header">from GlycoEpitope</div>
          <div class="card-body">
            <div class="py-3">Data not found</div>
          </div>
        </div>
      `;
    }
  } else if (this.source == "glycoprotein"){
    if (this.glyconnectArray.length) {
      if (this.glyconnectArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="card mt-3">
            <div class="card-header">from GlyConnect</div>
            <div class="card-body">
              ${this.glyconnectArray.map((row) => html`
                <ul style="display: inline-block;">
                  <li><small><a href="${row['url']}" target="_blank">${row['label']}</a></small></li>
                </ul>
              `)}
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="card mt-3">
        <div class="card-header">from GlyConnect</div>
          <div class="card-body">
            <div class="py-3">Data not found</div>
          </div>
        </div>
      `;
    }
  }
  }
}

customElements.define("glycosmos-literature", GlycosmosLiterature);
