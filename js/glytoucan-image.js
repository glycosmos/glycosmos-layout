import { LitElement, html } from "lit-element";

class GlytoucanImage extends LitElement {
  static get properties() {
    return {
      imageFile: { type: String },
      notation: { type: String },
      format: { type: String },
      accession: { type: String },
      wurcs: { type: String },
      imageApi: { type: String },
      host: { type: String },
      link: { type: String },
      image: { type: String, reflect: true },
      size: { type: String },
      sequence: { type: String }
    };
  }

  constructor() {
    super();
    this.imageFile = "https://image.glycosmos.org/";
    this.notation = "snfg";
    this.format = "png";
    this.accession = "";
    this.wurcs = "";
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/"
    this.host = "";
    this.size = ""
    this.sequence = ""
  }

  connectedCallback() {
    super.connectedCallback();
    this.getLink();
    this.getImage();
  }

  getLink() {
    this.link = "";
    if (this.host == "") {
      this.link = "https://glytoucan.org/Structures/Glycans/"
    } else {
      this.link = this.host  + "glycans/show?gtc_id=";
    }
  }

  getImage() {
    this.image = "";
    if (this.accession != "") {
      let url = this.imageFile + this.notation + '/' + this.format + '/' + this.accession;
      fetch(url).then((response) => {
        if (response.ok) {
          this.image = url;
        } else {
          this.runApi();
        }
      }).catch(error => {
        this.runApi();
        console.error('There has been a problem with your fetch operation:', error);
      });
    } else if (this.wurcs != "") {
      let url = this.imageApi + encodeURIComponent(this.wurcs);
      fetch(url).then((response) => {
        this.image = url;
      });
    }
  }

  runApi() {
    let url = this.imageApi + this.accession;
    fetch(url).then((response) => {
      this.image = url;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="d-flex justify-content-center">
        ${(this.accession != "")? html`
          <a href="${this.link}${this.accession}" target="_blank">
            <figure class="figure">
              ${this.image? html`
                <img src="${this.image}" class="figure-img img-fluid rounded" alt="${this.accession}" width="${this.size}" height="${this.size}">
              `:html`
                <div class="spinner-border my-2" style="width: 5rem; height: 5rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              `}
              ${this.sequence? html`
              `:html`
                <figcaption class="text-center">${this.accession}</figcaption>
              `}
            </figure>
          </a>
        `:html`
          ${(this.wurcs != "")? html`
            <figure class="figure">
              ${this.image? html`
                <img src="${this.image}" class="figure-img img-fluid rounded" alt="No Image" width="${this.size}" height="${this.size}">
              `:html`
                <div class="spinner-border my-2" style="width: 5rem; height: 5rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              `}
            </figure>
          `:html`
            Processing...
          `}
        `}
      </div>
    `;
  }
}

customElements.define("glytoucan-image", GlytoucanImage);
