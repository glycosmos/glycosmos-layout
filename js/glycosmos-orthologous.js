import { LitElement, html } from "lit-element";

class GlycosmosOrthologous extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      gene_id: { type: String },
      myArray: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.gene_id = "10678";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    fetch(this.domain + "glycosmos_ggdbs_orthologous?id=" + this.gene_id).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">Orthologous Gene</div>
        <div class="card-body">
          ${this.cbodyTemplate}
        </div>
      </div>
    `;
  }

  get cbodyTemplate() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          <div class="container mt-3">
            <div class="row row-cols-1 row-cols-md-2 row-cols-xl-3">
              ${this.myArray.map(row => html`
                <div class="col col-xl-4 mb-4">
                  <div class="card h-100">
                    <div class="card-header">${row.source_label}</div>
                    <div class="card-body">
                      ${this.replaceText(row.orthologous)}
                    </div>
                  </div>
                </div>
              `)}
            </div>
          </div>
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }
  replaceText(text) {
    let array = text.split(',');
    return html`
      <ul>
        ${array.map(row => html`
          ${row.indexOf('Protein') !== -1? html`
            <li>Protein: <a href="https://www.ncbi.nlm.nih.gov/protein/${row.replace('Protein:', '')}" target="_blank">${row.replace('Protein:', '')}</a></li>
          `:html`
            ${row.indexOf('mRNA') !== -1? html`
              <li>mRNA: <a href="https://www.ncbi.nlm.nih.gov/nuccore/${row.replace('mRNA:', '')}" target="_blank">${row.replace('mRNA:', '')}</a></li>
            `:``}
          `}
        `)}
      </ul>
    `;
  }
}

customElements.define("glycosmos-orthologous", GlycosmosOrthologous);
