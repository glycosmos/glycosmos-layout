import {LitElement, html} from 'lit-element';

class GlycosmosSubsume extends LitElement {
  static get properties() {
    return {
      api: { type: String },
      responseWArr: { type: Array },
      wkey: { type: String },
      wurcs: { type: String },
      rowspanArr: { type: Object },
      imageFile: { type: String },
      gtcid: { type: String},
      dtype: {type: Boolean}
    };
  }

constructor() {
    super();
    this.api = "https://api.test.glycosmos.org/"
    this.responseWArr = [];
    this.wkey="";
    this.wurcs="";
    this.rowspanArr={};
    this.imageFile = "https://image.beta.glycosmos.org/";
    this.gtcid = "";
    this.dtype = false;
  }

connectedCallback() {
    super.connectedCallback();
    this.getSubsumeJson()
  }

getSubsumeJson(){
  this.responseWArr = ["Loading..."];
  const url = this.api + 'sparqlist/subsumption-subsume?wurcs-key-uri=' +encodeURIComponent(this.wkey);
  fetch(url).then((response) => {
    return response.json();
  }).then((myJson) => {
    this.responseWArr = myJson;
  });
}


render() {
  return html `
  <style>
  .tooltips {
        width:90%;
        padding:4px;
        border:1px hidden;
        border-radius: 10px;

        background-color:	#EAD9FF;
        word-break:break-all;
        font-size:70%;
        /* text-align: center; */
  }
  .wurcshits{
    border-style: solid;
    border-color: #C299FF;
  }

  td{
    width:33%;
  }
  </style>
  <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
  <div  style="width:100%; padding:0; margin:0;">
  <table class="table table-bordered table-hover table-sm " style="width:100%; height:100%;  padding:0; margin:0; border-collapse: separate; border-spacing: 0;">
  <thead>
    <tr>
      <th scope="col" class="shadow-sm bg-white text-center" style="white-space: nowrap;　width:33%;">Monosaccharide composition</th>
      <th scope="col" class="shadow-sm bg-white text-center" style="white-space: nowrap; width:33%;">Glycosidic topology</th>
      <th scope="col" class="shadow-sm bg-white text-center" style="white-space: nowrap; width:33%;">Linkage defined structure</th>
    </tr>
  <tbody>
    ${this._processHtml()}
  </tbody>
  </table>
</div>

 `;
}



_processHtml() {
  if (this.responseWArr[0] == "Loading..."){
    return html `<div class="spinner-border" style="width: 5rem; height: 5rem; " role="status">
      <span class="sr-only">Loading...</span>
    </div>`;
  }else if (this.responseWArr[this.wkey] == undefined) {

    return html `<td colspan="3" style="text-align:center; margin:0">Not found</td>`;
  }else{
    // calculate the number of linkage define saccharide for each monosacchiride composition
    this.link_num();
    const base_img = this.api + "wurcs2image/0.8.0/png/binary/";
    var arr = this.responseWArr;

    if(this.dtype && this.gtcid){
      // subsumptionの中にターゲットが存在するかの確認（indexをboolフラグとしてl127で利用）
      var index = false;
      arr[this.wkey].map((mono) =>{
          if(arr[mono.key]){
            mono.url == this.gtcid
              ? !index ? index = true : ''
              : arr[mono.key].map((topo) =>{
                  if(arr[topo.key]){
                    topo.url == this.gtcid
                      ? !index ? index = true : ''
                      : arr[topo.key].map((link) =>{
                          link.url == this.gtcid
                            ? !index ? index = true : ''
                            : !index ? index = false : ''
                        }
                      )
                  }
                }
              )
          }
        }
      )
      
      // subsumptionの中にターゲットが無い場合はSubsumed ompositionのセルを結合させてNOT Found 表示
      if(!index) return html`<tr><td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center" rowspan="2"><h5>Not found</h5></td></tr>`
      var export_html =  html`${arr[this.wkey].map((mono) => html`
        ${(mono.type.indexOf('Monosaccharide') > -1)
          ? html`${(mono.url == this.gtcid)
            // Monosaccharide にターゲットがある場合
            ? html`${(!arr[mono.key])
              ? html`<tr><td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center"><h5>Not found</h5></td><td class="text-center"><h5>Not found</h5></td></tr>`
              : html`${arr[mono.key].map((topo, i) => html`
                ${(!arr[topo.key])
                  ? html`${(i > 0)
                    ? html`<tr><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`
                    : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`
                  }`
                  : html`${(i > 0)
                    ? html`${arr[topo.key].map((link, j) => html`
                      ${(j > 0)
                        ? html`<tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>`
                        : html`<tr><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center" >${this.template(link, topo.key)}</td></tr>`
                      }
                    `)}`
                    : html`${arr[topo.key].map((link, j) => html`
                      ${(j > 0)
                        ? html`<tr><td class="text-center">${this.template(link, topo.key)}</td></tr>`
                        : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center " >${this.template(link, topo.key)}</td></tr>`
                      }
                    `)}`
                  }`
                }
              `)}`
            }`
            // Monosaccharide にターゲットが無い場合
            : html`${(!arr[mono.key])
              // Topology が存在しない場合
              ? html``
              // Topology が存在する場合
              : html`${arr[mono.key].map((topo, i) => html`
                ${(!arr[topo.key])
                  // Linkage が存在しない場合
                  ? html`${(topo.url == this.gtcid)
                    // Topologyにターゲットがある場合
                    ? html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`
                    // Topologyにターゲットがなく存在しないLinkageに関してはoutput無し
                    : html``
                  }`
                  // Linkage が存在する場合
                  : html`${(topo.url == this.gtcid)
                    // Topologyにターゲットがある場合
                    ? html`${arr[topo.key].map((link, j) => html`
                      ${(j > 0)
                        // Linkageのindexによって列の作り方が異なるための場合分け
                        ? html`<tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>`
                        : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center " >${this.template(link, topo.key)}</td></tr>`
                      }
                    `)}`
                    // Linkageにターゲットがある場合
                    : html`${arr[topo.key].map((link, j) => html`
                      ${(link.url == this.gtcid)
                        // ターゲットであるLinkageに関連した列のみoutput
                        ? html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center " >${this.template(link, topo.key)}</td></tr>`
                        : html``
                      }
                    `)}`
                  }`
                }
              `)}`
            }`
          }`
          : html`
            ${(!arr[mono.key])
              ? html`<tr><td class="text-center"><h5>Not found</h5></td><td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center"><h5>Not found</h5></td></tr>`
              : html`${arr[mono.key].map((topo, i) => html`
                ${(i > 0)
                  ? html`<tr><td class="text-center">${this.template(topo, mono.key)}</td></tr>`
                  : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">Not found</td><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.key)}</td><td class="text-center">${this.template(topo, mono.key)}</td></tr>`
                }
              `)}`
            }
          `
        }
      `)}`
      return export_html;

      // Note: 2021/6/22 一つ下のreturn文を書き換える
      // Note: 2021/6/29 書き換え済み and テンプレート
      return html`${ arr[this.wkey].map((mono) => html`
        ${(mono.type.indexOf('Monosaccharide') > -1)
          ? html`${(!arr[mono.key])
            ? html`<tr><td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center"><h5>Not found</h5></td><td class="text-center"><h5>Not found</h5></td></tr>`
            : html`${arr[mono.key].map((topo, i) => html`
                ${(!arr[topo.key])
                  ? html`${(i > 0)
                    ? html`<tr><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`
                    : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`
                  }`
                  : html`${(i > 0)
                    ? html`${arr[topo.key].map((link, j) => html`
                      ${(j > 0)
                        ? html`<tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>`
                        : html`<tr><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center" >${this.template(link, topo.key)}</td></tr>`
                      }
                    `)}`
                    : html`${arr[topo.key].map((link, j) => html`
                      ${(j > 0)
                        ? html`<tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>`
                        : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center " >${this.template(link, topo.key)}</td></tr>`
                      }
                    `)}`
                  }`
                }
              `)}`
            }`
          : html`
            ${(!arr[mono.key])
              ? html`<tr><td class="text-center"><h5>Not found</h5></td><td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center"><h5>Not found</h5></td></tr>`
              : html`${arr[mono.key].map((topo, i) => html`
                ${(i > 0)
                  ? html`<tr><td class="text-center">${this.template(topo, mono.key)}</td></tr>`
                  : html`<tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">Not found</td><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.key)}</td><td class="text-center">${this.template(topo, mono.key)}</td></tr>`
                }
              `)}`
            }
          `
        }
      `)}`
    }else{
      return html `${arr[this.wkey].map((mono) => html `
      ${(mono.type.indexOf('Monosaccharide')>-1) ? html ` <!-- if base composition subsumes monosaccharide(normal) -->
              ${(!arr[mono.key]) ? html `     
              <tr><td class="text-center">${this.template(mono, this.wkey)}</td>
              <td class="text-center"><h5>Not found</h5></td><td class="text-center"><h5>Not found</h5></td></tr>
            `:html `
              ${arr[mono.key].map((topo, i) => html `
                ${(!arr[topo.key]) ? html `
                  ${(i>0) ? html `
                  <tr><td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>
                  `:html `
                    <tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td>
                    <td class="text-center">${this.template(topo, mono.key)}</td><td class="text-center">Not found</td></tr>`}
                ` :html `
                    ${(i>0) ? html `
                      ${arr[topo.key].map((link, j) => html `
                        ${(j>0) ? html `
                          <tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>
                          `: html`
                          <tr><td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center" >${this.template(link, topo.key)}</td></tr>
                          `}`)}
                    `:html `
                      ${arr[topo.key].map((link, j) => html `
                      ${(j>0) ? html `
                        <tr><td class="text-center" >${this.template(link, topo.key)}</td></tr>
                        `: 
                        html`
                        <tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.wkey)}</td>
                        <td rowspan =${arr[topo.key].length} class="text-center">${this.template(topo, mono.key)}</td><td class="text-center " >${this.template(link, topo.key)}</td></tr>
                        `}`)}
                  `}`}
          `)}`}
      `:html`   <!-- if base composition subsumes topology -->
      ${(!arr[mono.key]) ? html `
          <tr><td class="text-center"><h5>Not found</h5></td>
          <td class="text-center">${this.template(mono, this.wkey)}</td><td class="text-center"><h5>Not found</h5></td></tr>
          `:html ` <!-- if linkage defined composition is found -->
            ${arr[mono.key].map((topo, i) => html `
            ${(i>0) ? html `
              <tr><td class="text-center">${this.template(topo, mono.key)}</td></tr>
            `:html `
              <tr><td rowspan=${this.rowspanArr[mono.key]} class="text-center">Not found</td>
              <td rowspan=${this.rowspanArr[mono.key]} class="text-center">${this.template(mono, this.key)}</td><td class="text-center">${this.template(topo, mono.key)}</td></tr>
              `}
        `)}`}
      ` }
        
                <!-- end of loop ${(!arr[mono.key])} -->
      `)}`;
    }
    //end of retunr html
  }

}
// calculate the number of linkage define saccharide for each monosacchiride composition
link_num(){
  var arr = this.responseWArr;
  var row_int = 0;
  arr[this.wkey].map((mono) => {
    !arr[mono.key] ? console.log(`${mono.url} is undefined`)
    : arr[mono.key].map((topo) =>  {
      !arr[topo.key] ? row_int += 1: row_int += arr[topo.key].length
    });

    this.rowspanArr[mono.key] = row_int;
    row_int = 0;
  }  );


}


template(object, key){
  const base_img = this.api + "wurcs2image/0.8.0/png/binary/";
  if(object.wurcs == this.wurcs){
    var div_class = "mx-auto wurcshits"
  }else{
    var div_class = "mx-auto"
  }

    return html`
        <div id="${object.wurcs}" class="${div_class}">
            ${(object.url == "undefined")? html`
                <glytoucan-image wurcs=${object.wurcs} imageFile=${this.imageFile} imageApi=${base_img} @click="${this.handleClick}"></glytoucan-image>
                <h6>Not registered with GlyTouCan</h6>
            `:html`
                <glytoucan-image accession=${object.url} sequence="false" host="https://glycosmos.org/" imageFile=${this.imageFile} imageApi=${base_img} @click="${this.handleClick}"></glytoucan-image>
                <h6>GlyTouCan ID: <a href="https://glycosmos.org/glycans/show?gtc_id=${object.url}">${object.url}</a></h6>
            `}
        </div>
    `;
}
handleClick(e){
  if(e.path[5].id.indexOf('WURCS') != -1){
    if(e.path[5].children.length == 2 ){
      var wurcs_div = document.createElement('p');
      wurcs_div.innerHTML = e.path[5].id;
      wurcs_div.className = "mx-auto tooltips";
      e.path[5].appendChild(wurcs_div);
    }else{
      e.path[5].removeChild(e.path[5].children[2]);
    }
  }

}



}

customElements.define('glycosmos-subsume', GlycosmosSubsume);
