import { LitElement, html } from "lit-element";

class GlycosmosImage extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      uniprotId: { type: String },
      host: { type: String },
      imageFile: { type: String },
      imageApi: { type: String },
      myArray: { type: Array, reflect: true }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "gtc";
    this.uniprotId = "P06909";
    this.host = "";
    this.imageFile = "https://image.beta.glycosmos.org/";
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myArray = ["Loading..."];
    let url = this.domain + "glycosmos_glycoproteins_gtc?id=" + this.uniprotId;
    if (this.source == "pdb") {
      url = this.domain + "glycosmos_glycoproteins_pdb?id=" + this.uniprotId;
    } else if (this.source == "mcaw") {
      url = this.domain + "glycosmos_glycoproteins_mcaw?id=" + this.uniprotId;
    }
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        ${(this.source == "mcaw")? html`
          <div class="card-header">MCAW-DB (Glycan Recognition Profile) Images</div>
        `:html`
          ${(this.source == "pdb")? html`
            <div class="card-header">PDB Images</div>
          `:html`
            <div class="card-header">Glycan Images</div>
          `}
        `}
        <div class="card-body">
          ${this.cbodyTemplate}
        </div>
      </div>
    `;
  }

  get cbodyTemplate() {
    if (this.myArray.length) {
      if (this.myArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          ${this.source == "mcaw"? html`
              <p><a href="https://www.ncbi.nlm.nih.gov/pubmed/29859376" target="_blank">MCAW-DB</a> is a database whereby users can view the multiple alignment analysis results obtained from the <a href="https://rings.glycoinfo.org/mcaw/new" target="_blank">MCAW tool</a>, which can computationally find common glycan substructures among a group of glycan structures. The data in MCAW-DB are analysis results of glycan array experiments; those glycans having higher binding affinity to glycan binding proteins (GBPs) have been analyzed and can be viewed as “glycan recognition profiles”. Currently, this database provides published analysis results of glycan array data from the <a href="http://www.functionalglycomics.org/" target="_blank">Consortium for Functional Glycomics (CFG)</a>.</p>
              ${this.myArray[0]['mcaw_ids'].map(row => html`
                <div class="card mb-3">
                  <a href="https://mcawdb.glycoinfo.org/mcaw_public/${row}/image.png" target="_blank"><img class="card-img-top img-thumbnail" src="https://mcawdb.glycoinfo.org/mcaw_public/${row}/image.png"></a>
                  <div class="card-body">
                    <h5 class="card-title"><a href="https://mcawdb.glycoinfo.org/detail.html?${row}" target="_blank">${row}</a></h5>
                    <p class="card-text"><small class="text-muted">Image from <a href="https://mcawdb.glycoinfo.org/analysis-list.html" target="_blank">MCAW-DB</a></small></p>
                  </div>
                </div>
              `)}
          `:html`
            ${this.source == "pdb"? html`
                <div class="container mt-3">
                  <div class="row row-cols-2 row-cols-md-3 row-cols-xl-4">
                    ${this.myArray[0]['pdb_ids'].map(row => html`
                      <div class="col col-xl-3 mb-4">
                        <div class="card h-100">
                          <img class="card-img-top img-thumbnail" src="https://pdbj.org/rest/molmil-renderer?type=mine&id=${row.toLowerCase()}">
                          <div class="card-body">
                            <h5 class="card-title"><a href="https://pdbj.org/mine/summary/${row}" target="_blank">${row}</a></h5>
                            <p class="card-text">&copy; <a href="http://pdbj.org/" class="external mr-1" target="_blank" rel="nofollow">PDBj</a> <a href="http://creativecommons.org/licenses/by/4.0/deed.ja" target="_blank" rel="nofollow"><img src="https://i.creativecommons.org/l/by/4.0/80x15.png" alt="クリエイティブ・コモンズ 表示 4.0 国際 ライセンス" class="opacityTransition2" style="vertical-align: middle; padding-bottom: 1px;"></a></p>
                            <div class="text-right">
                              <a href="https://v.litemol.org/?loadFromCS=${row.toLowerCase()}" class="btn btn-primary btn-sm" target="_blank">Open LiteMol Viewer</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    `)}
                  </div>
                </div>
            `:html`
              ${this.myArray.map(data => html`
                ${data['database'] == "GlycoProtDB"? html`
                  <div class="mb-4"><p>GlyTouCan images of glycan extracted from <a href="https://acgg.asia/gpdb2/" target="_blank">GlycoProtDB</a></p></div>
                  <div class="container mt-3">
                    <div class="row row-cols-2 row-cols-md-3 row-cols-xl-4">
                      ${data['gtc_ids'].map(row => html`
                        <div class="col col-xl-3 mb-4">
                          <div class="card h-100">
                            <div class="card-body">
                              <glytoucan-image imageFile="${this.imageFile}" accession="${row}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
                            </div>
                          </div>
                        </div>
                      `)}
                    </div>
                  </div>
                `:``}
                ${data['database'] == "GlyConnect"? html`
                  <div class="mb-4"><p>GlyTouCan images of glycan extracted from <a href="https://glyconnect.expasy.org/" target="_blank">GlyConnect</a></p>
                  <div class="container mt-3">
                    <div class="row row-cols-2 row-cols-md-3 row-cols-xl-4">
                      ${data['gtc_ids'].map(row => html`
                        <div class="col col-xl-3 mb-4">
                          <div class="card h-100">
                            <div class="card-body">
                              <glytoucan-image imageFile="${this.imageFile}" accession="${row}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
                            </div>
                          </div>
                        </div>
                      `)}
                    </div>
                  </div>
                `:``}
                ${data['database'] == "GlyGen"? html`
                  <div class="mb-4"><p>GlyTouCan images of glycan extracted from <a href="https://www.glygen.org/" target="_blank">GlyGen</a></p>
                  <div class="container mt-3">
                    <div class="row row-cols-2 row-cols-md-3 row-cols-xl-4">
                      ${data['gtc_ids'].map(row => html`
                        <div class="col col-xl-3 mb-4">
                          <div class="card h-100">
                            <div class="card-body">
                              <glytoucan-image imageFile="${this.imageFile}" accession="${row}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
                            </div>
                          </div>
                        </div>
                      `)}
                    </div>
                  </div>
                `:``}
              `)}
            `}
          `}
        `;
      }
    }else{
      return html`
          <div class="py-3">Data not found</div>
        `
    }
  }
}

customElements.define("glycosmos-image", GlycosmosImage);
