import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosKeggreaction extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      source: { type: String },
      variable: { type: String },
      host: { type: String },
      reactionArray: { type: Array, reflect: true },
      enzymeArray: { type: Array, reflect: true },
      variable: { type: String },
      arrow: { type: String },
      imageApi: { type: String },
      imageFile: { type: String }
    };
  }

  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.source = "glycan";
    this.variable = "G44791DH";
    this.host = "";
    this.arrow = '';
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";
    this.imageFile = "https://image.beta.glycosmos.org/";
  }

  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    if(this.source == "glycan"){
      this.enzymeArray = ["Loading..."];
      let enzyme = this.domain + "glycosmos_glycan_kegg?gtc=" + this.variable;
      fetch(enzyme).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.enzymeArray = myJson;
      });
      this.reactionArray = ["Loading..."];
      let reaction = this.domain + "glycosmos_glycan_kegg_reaction?gtc=" + this.variable;
      fetch(reaction).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.reactionArray = myJson;
      });
    }else if(this.source == "glycogene"){
      this.reactionArray = ["Loading..."];
      let reaction = this.domain + "glycosmos_glycogenes_kegg_reaction?id=" + this.variable;
      fetch(reaction).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.reactionArray = myJson;
      });
    }
  }

  render() {
    if(this.source == "glycan"){
      return html`
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
        <div class="card">
          <div class="card-header">KEGG GLYCAN</div>
          <div class="card-body">
            ${this.enzyme}
            <div class="card mt-3">
              <div class="card-header">Reaction</div>
              <div class="card-body">
                ${this.reaction}
              </div>
            </div>
          </div>
        </div>
      `;
    }
    else if(this.source == "glycogene"){
      return html`
        <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
        <div class="card">
          <div class="card-body">
            <div class="card mt-3">
              <div class="card-header">Reaction</div>
              <div class="card-body">
                ${this.reaction}
              </div>
            </div>
          </div>
        </div>
      `;
    }
  }

  get enzyme() {
    if (this.enzymeArray.length) {
      if (this.enzymeArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
            ${this.enzymeArray.map((row) => html`
              <li><strong>KEGG ID</strong>: ${row['entry_id']}</li>
              ${this.enzymeArray[0]['enzyme'].length? html`
                <li>
                  <strong>Enzyme</strong>:
                  <ul>
                    ${this.enzymeArray[0]['enzyme'].map(row => html`
                      <a href="https://www.kegg.jp/dbget-bin/www_bget?ec:${row}" target="_blank">${row}</a>
                    `)}
                  </ul>
                </li>
              `:``}
            `)}
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }
  get reaction() {
    if (this.reactionArray.length) {
      if (this.reactionArray[0] == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
          ${this.reactionArray.map((row) => html`
            <div class="card">
              <div class="card-body">
                <h6 class="card-subtitle mb-2"><a href="https://www.kegg.jp/dbget-bin/www_bget?${row['reaction']}" target="_blank">${row['reaction']}</a></h6>
                <table class="table table-borderless">
                  <tbody>
                    <tr>
                      <td class="align-middle">
                        ${row.left_gtc.map((gtc) => html`
                          <div class="float-left"><glytoucan-image imageFile="${this.imageFile}" accession="${gtc}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image></div>
                        `)}
                      </td>
                      <td class="align-middle">
                        ${this.arrow? html`
                          ${unsafeHTML(this.arrow)}
                        `:html`
                          <h2 class="text-center">></h2>
                        `}
                      </td>
                      <td class="align-middle">
                        ${row.right_gtc.map((gtc) => html`
                          <div class="float-left"><glytoucan-image imageFile="${this.imageFile}" accession="${gtc}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image></div>
                        `)}
                      </td>
                    </tr>
                    <tr>
                      <td class="align-middle">
                        ${row.left_name.join(' + ')}
                      </td>
                      <td class="align-middle">
                      </td>
                      <td class="align-middle">
                        ${row.right_name.join(' + ')}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          `)}
        `;
      }
    } else {
      return html`
        <div class="py-3">Data not found</div>
      `;
    }
  }
}

customElements.define("glycosmos-keggreaction", GlycosmosKeggreaction);
