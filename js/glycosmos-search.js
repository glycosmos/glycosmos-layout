import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosSearch extends LitElement {
  static get properties() {
    return {
      wrapper: { type: String },
      sparqlet: { type: String },
      links: { type: Object },
      key: { type: String },
      db: { type: String },
      source: { type: String },
      glycogenes: { type: Number },
      glycogenes_url: { type: String },
      glycogenes_key: { type: String },
      glycoproteins: { type: Number },
      glycoproteins_url: { type: String },
      glycoproteins_key: { type: String },
      lectins: { type: Number },
      lectins_url: { type: String },
      lectins_key: { type: String },
      glycolipids: { type: Number },
      glycolipids_url: { type: String },
      glycolipids_key: { type: String },
      glycans: { type: Number },
      glycans_url: { type: String },
      glycans_key: { type: String },
      pathways: { type: Number, reflect: true },
      pathwaysData: { type: Object, reflect: true }
    };
  }

  constructor() {
    super();
    this.wrapper = "https://test.glycosmos.org/wrappers/index.json?url=";
    this.sparqlet = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.links = {
      "glycogenes": {
        "url": "https://test.glycosmos.org/",
        "img": ""
      },
      "glycoproteins": {
        "url": "https://test.glycosmos.org/",
        "img": ""
      },
      "lectins": {
        "url": "https://test.glycosmos.org/",
        "img": ""
      },
      "glycolipids": {
        "url": "https://test.glycosmos.org/",
        "img": ""
      },
      "glycans": {
        "url": "https://test.glycosmos.org/",
        "img": ""
      },
      "pathways": {
        "url": "https://test.glycosmos.org/pathways/cross_search?key=",
        "img": ""
      }
    };
    this.key = "";
    this.db = "All"
  }

  connectedCallback() {
    super.connectedCallback();
    if (this.db == "All") {
      this.getGlycogenes();
      this.getGlycoproteins();
      this.getLectins();
      this.getGlycolipids();
      this.getGlycans();
      this.getPathways();
    } else if (this.db == "GlycoGenes"){
      this.getGlycogenes();
    } else if (this.db == "Glycoproteins"){
      this.getGlycoproteins();
    } else if (this.db == "Lectins"){
      this.getLectins();
    } else if (this.db == "Glycolipids"){
      this.getGlycolipids();
    } else if (this.db == "Glycans"){
      this.getGlycans();
    } else if (this.db == "Pathways"){
      this.getPathways();
    }
  }

  getGlycogenes() {
    this.glycogenes = -1;
    if (this.key != "") {
      let columns = ["GeneSymbol", "gene_id", "ggdb_id", "fgdb_id", "kegg_id", "lm_id", "disease_name", "aliase_disease_names", "source_label"];
      let param = "FILTER(";
      columns.forEach(col => param += "CONTAINS(lcase(str(?" + col + ")),\'" + this.key.toLowerCase() + "\') %7C%7C ");
      param = param.slice(0, -8);
      param? param += ")" : "";
      fetch(this.wrapper + "http://glycosmos-sparqlist:3000/sparqlist/api/glycosmos_glycogenes_list" + "&order=&limit=&offset=0&search=" + param).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.glycogenes = myJson.length;
        if (this.glycogenes == 1 && myJson[0]["ggdb_id"] != ""){
          this.source = "ggdb";
        } else if (this.glycogenes == 1 && myJson[0]["fgdb_id"] != ""){
          this.source = "fgdbs";
        } else if (this.glycogenes == 1){
          this.source =  "kegg";
        }
        if (this.glycogenes == 1){
          this.glycogenes_key = myJson[0]["gene_id"]
          this.glycogenes_url = this.links['glycogenes']['url'] + "glycogenes/show/" + this.source + "/" + this.glycogenes_key;
        } else {
          this.glycogenes_url = this.links['glycogenes']['url'] + "glycogenes/index?key=" + this.key;
        }
      });
    } else {
      this.glycogenes = 0;
    }
  }

  getGlycoproteins() {
    this.glycoproteins = -1;
    if (this.key != "") {
      let columns = ["protein_name", "uniprot_id", "gene_name", "scientific_name", "gtc", "disease_name", "aliase_disease_names", "go_id", "go"];
      let param = "FILTER(";
      columns.forEach(col => param += "CONTAINS(lcase(str(?" + col + ")),\'" + this.key.toLowerCase() + "\') %7C%7C ");
      param = param.slice(0, -8);
      param? param += ")" : "";
      fetch(this.wrapper + "http://glycosmos-sparqlist:3000/sparqlist/api/glycosmos_glycoproteins_list" + "&order=&limit=&offset=0&search=" + param).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.glycoproteins = myJson.length;
        if (this.glycoproteins == 1){
          this.glycoproteins_key = myJson[0]["uniprot_id"]
          this.glycoproteins_url = this.links['glycoproteins']['url'] + "glycoproteins/show/uniprot/" + this.glycoproteins_key;
        } else {
          this.glycoproteins_url = this.links['glycoproteins']['url'] + "glycoproteins/index?key=" + this.key;
        }
      });
    } else {
      this.glycoproteins = 0;
    }
  }

  getLectins() {
    this.lectins = -1;
    if (this.key != "") {
      let columns = ["protein_name", "uniprot_id", "pdb_ids", "lfdb_ids", "monosaccharide_specificitys", "scientific_name", "mcaw_ids", "go_id", "go"];
      let param = "FILTER(";
      columns.forEach(col => param += "CONTAINS(lcase(str(?" + col + ")),\'" + this.key.toLowerCase() + "\') %7C%7C ");
      param = param.slice(0, -8);
      param? param += ")" : "";
      fetch(this.wrapper + "http://glycosmos-sparqlist:3000/sparqlist/api/glycosmos_lectins_list" + "&order=&limit=&offset=0&search=" + param).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.lectins = myJson.length;
        if (this.lectins == 1){
          this.lectins_key = myJson[0]["uniprot_id"];
          this.lectins_url = this.links['lectins']['url'] + "glycoproteins/show/uniprot/" + this.lectins_key;
        } else {
          this.lectins_url = this.links['lectins']['url'] + "lectins/index?key=" + this.key;
        }
      });
    } else {
      this.lectins = 0;
    }
  }

  getGlycolipids() {
    this.glycolipids = -1;
    if (this.key != "") {
      let columns = ["lm_id", "common_name", "lipidbank", "kegg", "pubchem", "chebi", "formula"];
      let param = "FILTER(";
      columns.forEach(col => param += "CONTAINS(lcase(str(?" + col + ")),\'" + this.key.toLowerCase() + "\') %7C%7C ");
      param = param.slice(0, -8);
      param? param += ")" : "";
      fetch(this.sparqlet + "glycosmos_glycolipids_search" + "?order=ASC(lcase(%3Flm_id))&limit=&offset=0&search=" + param).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.glycolipids = myJson.length;
        if (this.glycolipids == 1){
          this.glycolipids_key = myJson[0]["lm_id"]
          this.glycolipids_url = this.links['glycolipids']['url'] + "glycolipids/show/" + this.glycolipids_key;
        } else {
          this.glycolipids_url = this.links['glycolipids']['url'] + "glycolipids/index?key=" + this.key;
        }
      });
    } else {
      this.glycolipids = 0;
    }
  }

  getGlycans() {
    this.glycans = -1;
    if (this.key != "") {
      let columns = ["glycan", "iupac_condensed", "mass", "motif", "subsumption", "taxon", "tissue"];
      let param = "FILTER(";
      columns.forEach(col => param += "CONTAINS(lcase(str(?" + col + ")),\'" + this.key.toLowerCase() + "\') %7C%7C ");
      param = param.slice(0, -8);
      param? param += ")" : "";
      fetch(this.wrapper + "http://glycosmos-sparqlist:3000/sparqlist/api/glycosmos_glycans_list" + "&order=&limit=&offset=0&search=" + param).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.glycans = myJson.length;
        if (this.glycans == 1){
          this.glycans_key = myJson[0]["glycan"]
          this.glycans_url = this.links['glycans']['url'] + "glycans/show?gtc_id=" + this.glycans_key;
        } else {
          this.glycans_url = this.links['glycans']['url'] + "glycans/index?key=" + this.key;
        }
      });
    } else {
      this.glycans = 0;
    }
  }

  getPathways() {
    this.pathways = -1;
    this.pathwaysData = {};
    if (this.key != "") {
      fetch(this.sparqlet + "pathway_search_by_uniprot" + "?filter=FILTER(CONTAINS(lcase(str(?name)), '" + this.key.toLowerCase() + "'))" ).then((response) => {
        return response.json();
      }).then((ids) => {
        if (ids.length) {
          ids.forEach(id => {
            fetch(this.sparqlet + "pathway_search_by_protein" + "?key=" + id['Uniprot']).then((response) => {
              return response.json();
            }).then((myJson) => {
              myJson.forEach(data => {
                if (Object.keys(this.pathwaysData).indexOf(data["organism"]) == -1) {
                  this.pathwaysData[data["organism"]] = [data["pathway_ID"]];
                } else {
                  if (this.pathwaysData[data["organism"]].indexOf(data["pathway_ID"]) == -1) {
                    this.pathwaysData[data["organism"]].push(data["pathway_ID"]);
                  }
                }
              });
              let count = 0;
              Object.keys(this.pathwaysData).map(data => {
                count += this.pathwaysData[data].length;
              });
              this.pathways = count;
            });
          });
        } else {
          fetch(this.sparqlet + "pathway_search_by_protein" + "?key=" + this.key).then((response) => {
            return response.json();
          }).then((myJson) => {
            myJson.forEach(data => {
              if (Object.keys(this.pathwaysData).indexOf(data["organism"]) == -1) {
                this.pathwaysData[data["organism"]] = [data["pathway_ID"]];
              } else {
                if (this.pathwaysData[data["organism"]].indexOf(data["pathway_ID"]) == -1) {
                  this.pathwaysData[data["organism"]].push(data["pathway_ID"]);
                }
              }
            });
            let count = 0;
            Object.keys(this.pathwaysData).map(data => {
              count += this.pathwaysData[data].length;
            });
            this.pathways = count;
          });
        }
      });
    } else {
      this.pathways = 0;
    }
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <style>
        .loader-wrap {
          z-index: 9999;
          position: fixed;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
          background-color: gray;
          opacity: 0.8;
        }
      </style>
      <div class="container-fluid">
        ${this.key? html`
          <div class="alert alert-primary" role="alert">Search results for <strong>${this.key}</strong></div>
        `:``}
        <div class="row">
          ${(this.db != "Pathways")? html`
            <div class="col-xl">
              <div class="card border-primary">
                <div class="card-header">Genes/Proteins/Lipids</div>
                <div class="card-body">
                ${(this.db == "GlycoGenes" || this.db == "All")? html`
                  ${this.glycogeneshow}
                `:``}
                ${(this.db == "Glycoproteins" || this.db == "All")? html`
                  ${this.glycoproteinshow}
                `:``}
                ${(this.db == "Lectins" || this.db == "All")? html`
                  ${this.lectinshow}
                `:``}
                ${(this.db == "Glycolipids" || this.db == "All")? html`
                  ${this.glycolipidshow}
                `:``}
                ${(this.db == "Glycans" || this.db == "All")? html`
                  ${this.glycanshow}
                `:``}
                </div>
              </div>
            </div>
          `:``}
          ${(this.db == "Pathways" || this.db == "All")? html`
            <div class="col-xl">
              <div class="card border-primary">
                <div class="card-header">Pathways/Diseases</div>
                <div class="card-body">
                  ${this.pathwayshow}
                </div>
              </div>
            </div>
          `:``}
        </div>
      </div>
    `;
  }

  get glycogeneshow() {
    return html`
      <h5 class="card-title">
        ${this.glycogenes > 0? html`
          <a href="${this.glycogenes_url}" target="_blank"><img src="${this.links['glycogenes']['img']}" alt="GlyCosmos GlycoGenes" class="mr-1" height="50">GlyCosmos GlycoGenes</a>
          <span class="badge badge-primary" style="height: 1.2rem;">${this.glycogenes}</span>
        `:html`
          <img src="${this.links['glycogenes']['img']}" alt="GlyCosmos GlycoGenes" class="mr-1" height="50">GlyCosmos GlycoGenes
          <span class="badge badge-secondary" style="height: 1.2rem;">
            ${this.glycogenes != -1? html`
              ${this.glycogenes}
            `:html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 0.7rem; height: 0.7rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `}
          </span>
        `}
      </h5>
    `;
  }
  get glycoproteinshow() {
    return html`
      <h5 class="card-title">
        ${this.glycoproteins > 0? html`
          <a href="${this.glycoproteins_url}" target="_blank"><img src="${this.links['glycoproteins']['img']}" alt="GlyCosmos Glycoproteins" class="mr-1" height="50">GlyCosmos Glycoproteins</a>
          <span class="badge badge-primary" style="height: 1.2rem;">${this.glycoproteins}</span>
        `:html`
          <img src="${this.links['glycoproteins']['img']}" alt="GlyCosmos Glycoproteins" class="mr-1" height="50">GlyCosmos Glycoproteins
          <span class="badge badge-secondary" style="height: 1.2rem;">
            ${this.glycoproteins != -1? html`
              ${this.glycoproteins}
            `:html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 0.7rem; height: 0.7rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `}
          </span>
        `}
      </h5>
    `;
  }
  get lectinshow() {
    return html`
      <h5 class="card-title">
        ${this.lectins > 0? html`
          <a href="${this.lectins_url}" target="_blank"><img src="${this.links['lectins']['img']}" alt="GlyCosmos Lectins" class="mr-1" height="50">GlyCosmos Lectins</a>
          <span class="badge badge-primary" style="height: 1.2rem;">${this.lectins}</span>
        `:html`
          <img src="${this.links['lectins']['img']}" alt="GlyCosmos Lectins" class="mr-1" height="50">GlyCosmos Lectins
          <span class="badge badge-secondary" style="height: 1.2rem;">
            ${this.lectins != -1? html`
              ${this.lectins}
            `:html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 0.7rem; height: 0.7rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `}
          </span>
        `}
      </h5>
    `;
  }
  get glycolipidshow() {
    return html`
      <h5 class="card-title">
        ${this.glycolipids > 0? html`
          <a href="${this.glycolipids_url}" target="_blank"><img src="${this.links['glycolipids']['img']}" alt="GlyCosmos Glycolipids" class="mr-1" height="50">GlyCosmos Glycolipids</a>
          <span class="badge badge-primary" style="height: 1.2rem;">${this.glycolipids}</span>
        `:html`
          <img src="${this.links['glycolipids']['img']}" alt="GlyCosmos Glycolipids" class="mr-1" height="50">GlyCosmos Glycolipids
          <span class="badge badge-secondary" style="height: 1.2rem;">
            ${this.glycolipids != -1? html`
              ${this.glycolipids}
            `:html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 0.7rem; height: 0.7rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `}
          </span>
        `}
      </h5>
    `;
  }
  get glycanshow() {
    return html`
      <h5 class="card-title">
        ${this.glycans > 0? html`
          <a href="${this.glycans_url}" target="_blank"><img src="${this.links['glycans']['img']}" alt="GlyCosmos Glycans" class="mr-1" height="50">GlyCosmos Glycans</a>
          <span class="badge badge-primary" style="height: 1.2rem;">${this.glycans}</span>
        `:html`
          <img src="${this.links['glycans']['img']}" alt="GlyCosmos Glycans" class="mr-1" height="50">GlyCosmos Glycans
          <span class="badge badge-secondary" style="height: 1.2rem;">
            ${this.glycans != -1? html`
              ${this.glycans}
            `:html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 0.7rem; height: 0.7rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `}
          </span>
        `}
      </h5>
    `;
  }
  get pathwayshow() {
    return html`
      <h5 class="card-title">
        ${this.pathways > 0? html`
          <img src="${this.links['pathways']['img']}" alt="GlyCosmos Pathways" class="mr-1" height="50">GlyCosmos Pathways
          <span class="badge badge-primary" style="height: 1.2rem;">${this.pathways}</span>
          <ul class="list-unstyled mt-3 ml-3">
            ${Object.keys(this.pathwaysData).map(data => html`
              <li>
                <h6>
                  <a href="${this.links['pathways']['url'] + this.key}&organism=${data}" target="_blank">${data}</a>
                  <span class="badge badge-primary" style="height: 1.0rem;">${this.pathwaysData[data].length}</span>
                </h6>
              </li>
            `)}
          </ul>
        `:html`
          <img src="${this.links['pathways']['img']}" alt="GlyCosmos Pathways" class="mr-1" height="50">GlyCosmos Pathways
          <span class="badge badge-secondary" style="height: 1.2rem;">
            ${this.pathways != -1? html`
              ${this.pathways}
            `:html`
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 0.7rem; height: 0.7rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            `}
          </span>
        `}
      </h5>
    `;
  }
}

customElements.define("glycosmos-search", GlycosmosSearch);
