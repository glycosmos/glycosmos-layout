import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosTable extends LitElement {
  static get properties() {
    return {
      listSparqlet: { type: String },
      countSparqlet: { type: String },
      columns: { type: Object },
      links: { type: Object },
      ifLinks: { type: Object },
      orderBool: { type: Boolean },
      defaultSearch: { type: String },
      defaultColumnSearch: { type: Object },
      rangeText: { type: String },
      hiddenColumns: { type: Array },
      stickyLeft: { type: String },
      stickyTop: { type: String },
      imageFile: { type: String },
      imageApi: { type: String },
      orderCol: { type: String },
      selectLists: { type: Object, reflect: true },
      myArray: { type: Array, reflect: true },
      limit: { type: Number },
      page: { type: Number },
      count: { type: Number, reflect: true },
      load: { type: String },
      footerHidden: { type: Boolean, reflect: true },
      hidden: { type: Boolean },
      host: { type: String }
    };
  }

  constructor() {
    super();
    this.listSparqlet = "https://test.sparqlist.glycosmos.org/sparqlist/api/glycosmos_glycoproteins_filter";
    this.countSparqlet = "";
    this.host = "";
    this.columns = {
      "protein_name": {
        "name": "Protein Name",
        "type": "string"
      },
      "uniprot_id": {
        "name": "UniProt ID",
        "type": "string"
      },
      "gene_name": {
        "name": "Gene Symbol",
        "type": "string"
      },
      "scientific_name": {
        "name": "Organism",
        "type": "string"
      },
      "positions": {
        "name": "No. of Glycosylation Sites",
        "type": "number"
      },
      "mcaw_ids": {
        "name": "MCAW IDs",
        "type": "Array"
      }
    };
    this.links = {
      "protein_name": {
        "url": "https://glycosmos.org/glycoproteins/show?source=uniprot&uniprot_id=",
        "key": "uniprot_id"
      },
      "uniprot_id": {
        "url": "https://purl.uniprot.org/uniprot/",
        "key": "uniprot_id"
      },
      "mcaw_ids": {
        "url": "https://mcawdb.glycoinfo.org/detail.html?"
      }
    };
    this.ifLinks = {};
    this.orderBool = false;
    this.defaultSearch = "";
    this.defaultColumnSearch = {};
    this.rangeText = ",,"
    this.hiddenColumns = [];
    this.stickyLeft = "";
    this.stickyTop = "";
    this.imageFile = "https://image.beta.glycosmos.org/";
    this.imageApi = "https://test.api.glycosmos.org/wurcs2image/0.8.0/png/binary/";

    this.searchArray = {};
    this.numString = 0;
    this.limit = 10;
    this.page = 1;
    this.search = "";
    this.load = "";
    this.footerHidden = false;
    this.hidden = true;
    this.orSearch = "";
    this.andSearch = "";
  }

  connectedCallback() {
    super.connectedCallback();
    this.columnNames = Object.keys(this.columns);
    this.orderCol = this.columnNames[0];
    this.columnNames.forEach(col => this.searchArray[col] = "");
    this.columnNames.forEach(col => this.columns[col].type == "string" || this.columns[col].type == "Array" || this.columns[col].type == "Image"? this.numString++ : console.log());
    this.orderBool = !this.orderBool;
    this.paramBreak = this.listSparqlet.indexOf('?') == -1? "?" : "&";
    this.defaultSearch? this.searchTable(this.defaultSearch) : console.log();
    Object.keys(this.defaultColumnSearch).map(col => this.searchColumn(col, this.defaultColumnSearch[col]));
    this.initSelectLists();
    this.getJson();
    this.getCount();
  }

  initSelectLists() {
    this.selectLists = {};
    this.columnNames.forEach(col => this.columns[col].type == "select"? this.selectLists[col] = ["Loading..."] : console.log());
    fetch(this.listSparqlet + this.paramBreak + "limit=&offset=0&search=").then((response) => {
      return response.json();
    }).then((myJson) => {
      Object.keys(this.selectLists).forEach(col => this.selectLists[col] = myJson.map(element => element[col]).filter(function (x, i, self) {
        return self.indexOf(x) === i
      }));
    });
  }

  getJson() {
    this.myArray = ["Loading..."];
    this.offset = this.limit * (this.page - 1);
    let order;
    this.orderBool? order = "ASC" : order = "DESC";
    let newCol;
    if (this.columns[this.orderCol].type == "number" || this.columns[this.orderCol].type == "date") {
      newCol = "%3F" + this.orderCol;
    } else {
      newCol = "lcase(%3F" + this.orderCol + ")";
    }
    let searchText;
    this.numString? searchText = "&search=" + this.search : searchText = "";
    fetch(this.listSparqlet + this.paramBreak + "order=" + order + "(" + newCol + ")&limit=LIMIT%20" + this.limit + "&offset=" + this.offset + searchText + "&range=" + this.rangeText).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myArray = myJson;
    });
  }

  getCount() {
    this.count = 0;
    if (this.countSparqlet) {
      fetch(this.countSparqlet + this.paramBreak + "search=" + this.search + "&range=" + this.rangeText).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.count = myJson['count'];
      });
    } else {
      let order;
      this.orderBool? order = "ASC" : order = "DESC";
      let newCol;
      if (this.columns[this.orderCol].type == "number" || this.columns[this.orderCol].type == "date") {
        newCol = "%3F" + this.orderCol;
      } else {
        newCol = "lcase(%3F" + this.orderCol + ")";
      }
      let searchText;
      this.numString? searchText = "&search=" + this.search : searchText = "";
      fetch(this.listSparqlet + this.paramBreak + "order=" + order + "(" + newCol + ")&limit=&offset=0" + searchText + "&range=" + this.rangeText).then((response) => {
        return response.json();
      }).then((myJson) => {
        this.count = myJson.length;
      });
    }
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <style>
        .loader-wrap {
          z-index: 9999;
          position: fixed;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
          background-color: gray;
          opacity: 0.8;
        }
        pre {
          white-space: pre-wrap ;
        }
        ${this.stickyLeft? 'tbody tr td:first-of-type { background-color: white; position: sticky; left: ' + this.stickyLeft + '; }':''}
      </style>
      <div class="container-fluid">
        ${this.load? html`
          <div class="d-flex justify-content-center loader-wrap">
            <div class="spinner-border" style="margin-top: calc(50vh - 4rem); width: 8rem; height: 8rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `:html``}
        ${this.hiddenColumns.length == this.columnNames.length? html`
          <button type="button" class="btn btn-primary my-3" @click="${() => { this.hiddenColumns = []; }}">Show table</button>
        `:html`
          <div class="d-flex justify-content-between align-items-end my-3">
            ${this.selectTemplate}
            ${this.hiddenTemplate}
            ${this.searchTemplate}
          </div>
          <table class="table table-bordered table-hover table-sm" style="border-collapse: separate; border-spacing: 0;">
            <thead>
              ${this.theadTemplate}
            </thead>
            <tbody>
              ${this.tbodyTemplate}
            </tbody>
          </table>
          ${!this.footerHidden? html`
            <div class="d-flex justify-content-between mb-4">
              ${this.infoTemplate}
              ${this.downloadTemplate}
              ${this.pageTemplate}
            </div>
          `:html``}
        </div>
      `}
    `;
  }

  get selectTemplate() {
    return html`
      <div class="form">
        <div class="form-row align-items-center my-1" style="margin-left: 0;">
          Show
          <div class="col-auto">
            <select class="custom-select custom-select-sm" @change="${(e) => { this.limit = Number(e.target.value); this.page = 1; this.getJson(); }}">
              <option selected>10</option>
              <option>25</option>
              <option>50</option>
              <option>100</option>
            </select>
          </div>
          entries
        </div>
        <small class="text-muted">To search, enter a keyword in the text box and press Enter (return).</small>
      </div>
    `;
  }

  get hiddenTemplate() {
    return html`
      ${this.hiddenColumns.length? html`
        ${this.hidden? html`
          <button type="button" class="btn btn-outline-primary ml-2" @click="${() => { this.hidden = !this.hidden; }}">Show hidden columns</button>
        `:html`
          ${this.numString? html`
            <button type="button" class="btn btn-primary ml-2" @click="${() => { this.hidden = !this.hidden; this.searchTable(""); }}">Hide columns</button>
          `:html`
            <button type="button" class="btn btn-primary ml-2" @click="${() => { this.hidden = !this.hidden; }}">Hide columns</button>
          `}
        `}
      `:html``}
    `;
  }

  get searchTemplate() {
    return html`
      ${this.numString? html`
        ${!this.hidden || !this.hiddenColumns.length? html`
          <div class="form-inline ml-2">
            <label class="mr-2">Search</label>
            ${this.defaultSearch? html`
              <input class="form-control" type="text" placeholder="across columns" @change="${(e) => { this.searchTable(e.target.value); }}" value="${this.defaultSearch}">
            `:html`
              <input class="form-control" type="text" placeholder="across columns" @change="${(e) => { this.searchTable(e.target.value); }}">
            `}
          </div>
        `:html``}
      `:html``}
    `;
  }

  searchTable(key) {
    if (key) {
      let param = "FILTER("
      for (let name in this.columns) {
        if (this.columns[name].type == "string" || this.columns[name].type == "Array" || this.columns[name].type == "Image" || this.columns[name].type == "Hash") {
          param += "CONTAINS(lcase(str(?" + name + ")),\'" + key.toLowerCase() + "\') %7C%7C ";
        }
      }
      param = param.slice(0, -8)
      param? param += ")" : "";
      this.orSearch = param;
    } else {
      this.orSearch = "";
    }
    this.runSearch();
  }

  runSearch() {
    this.search = this.orSearch? this.orSearch + "%20" + this.andSearch : this.andSearch? this.andSearch : "";
    this.page = 1;
    this.getJson();
    this.getCount();
  }

  get theadTemplate() {
    return html`
      <tr>
        ${this.columnNames.map(col => html`
          ${!this.hidden || this.hiddenColumns.indexOf(col) == -1? html`
            <th scope="col" class="text-center ${this.stickyTop? 'align-middle sticky-top shadow-sm bg-white' : 'align-middle'}" style="top: ${this.stickyTop? this.stickyTop : '0px'};">
              ${this.orderCol == col? html`
                ${this.orderBool? html`
                  ${this.columns[col].type == "number"? html`
                    <input class="form-control form-control-sm" type="text" placeholder="Minimum" @change="${(e) => { this.rangeAbove(col, e.target.value); }}" value="${this.rangeText.split(',')[0]}">
                    <<div class="btn-group dropdown"><button class="btn btn-link dropdown-toggle" @click="${() => { this.sortTable(col); }}"><strong>${unsafeHTML(this.columns[col].name)}</strong></button></div><
                    <input class="form-control form-control-sm" type="text" placeholder="Maximum" @change="${(e) => { this.rangeBelow(col, e.target.value); }}" value="${this.rangeText.split(',')[2]}">
                  `:html`
                    <div class="btn-group dropdown"><button class="btn btn-link dropdown-toggle" @click="${() => { this.sortTable(col); }}"><strong>${unsafeHTML(this.columns[col].name)}</strong></button></div>
                  `}
                `:html`
                  ${this.columns[col].type == "number"? html`
                    <input class="form-control form-control-sm" type="text" placeholder="Minimum" @change="${(e) => { this.rangeAbove(col, e.target.value); }}" value="${this.rangeText.split(',')[0]}">
                    <<div class="btn-group dropup"><button class="btn btn-link dropdown-toggle" @click="${() => { this.sortTable(col); }}"><strong>${unsafeHTML(this.columns[col].name)}</strong></button></div><
                    <input class="form-control form-control-sm" type="text" placeholder="Maximum" @change="${(e) => { this.rangeBelow(col, e.target.value); }}" value="${this.rangeText.split(',')[2]}">
                  `:html`
                    <div class="btn-group dropup"><button class="btn btn-link dropdown-toggle" @click="${() => { this.sortTable(col); }}"><strong>${unsafeHTML(this.columns[col].name)}</strong></button></div>
                  `}
                `}
              `:html`
                ${this.columns[col].type == "string" || this.columns[col].type == "str" || this.columns[col].type == "sortOnly" || this.columns[col].type == "date" || this.columns[col].type == "array" || this.columns[col].type == "Array" || this.columns[col].type == "Image" || this.columns[col].type == "select"? html`
                  <button class="btn btn-link" @click="${() => { this.sortTable(col); }}"><strong>${unsafeHTML(this.columns[col].name)}</strong></button>
                `:html`
                  ${this.columns[col].type == "number"? html`
                    <input class="form-control form-control-sm" type="text" placeholder="Minimum" @change="${(e) => { this.rangeAbove(col, e.target.value); }}" value="${this.rangeText.split(',')[0]}">
                    <<button class="btn btn-link" @click="${() => { this.sortTable(col); }}"><strong>${unsafeHTML(this.columns[col].name)}</strong></button><
                    <input class="form-control form-control-sm" type="text" placeholder="Maximum" @change="${(e) => { this.rangeBelow(col, e.target.value); }}" value="${this.rangeText.split(',')[2]}">
                  `:html`
                    <button class="btn"><strong>${unsafeHTML(this.columns[col].name)}</strong></button>
                  `}
                `}
              `}
              ${this.columns[col].type == "string" || this.columns[col].type == "Array" || this.columns[col].type == "Image" || this.columns[col].type == "Hash"? html`
                ${this.defaultColumnSearch.hasOwnProperty(col)? html`
                  <input class="form-control form-control-sm" type="text" placeholder="Search" @change="${(e) => { this.searchColumn(col, e.target.value); }}" value="${this.defaultColumnSearch[col]}">
                `:html`
                  <input class="form-control form-control-sm" type="text" placeholder="Search" @change="${(e) => { this.searchColumn(col, e.target.value); }}">
                `}
              `:html`
                ${this.columns[col].type == "select"? html`
                  <select class="custom-select custom-select-sm" @change="${(e) => { this.searchColumn(col, e.target.value); }}">
                    ${this.selectLists[col][0] == "Loading..."? html`
                      <option selected>Loading...</option>
                    `:html`
                      <option selected></option>
                    `}
                    ${this.selectLists[col].map(element => html`
                      <option>${element}</option>
                    `)}
                  </select>
                `:``}
              `}
            </th>
          `:``}
        `)}
      </tr>
    `;
  }

  sortTable(columnName) {
    this.orderBool = this.orderCol == columnName? !this.orderBool : true;
    this.orderCol = columnName;
    this.getJson();
  }

  searchColumn(columnName, key) {
    this.searchArray[columnName] = key;
    let param = "FILTER("
    for (let name in this.searchArray) {
      if (this.searchArray[name]) {
        param += "CONTAINS(lcase(str(?" + name + ")),\'" + this.searchArray[name].toLowerCase() + "\') %26%26 ";
      }
    }
    param = param.slice(0, -8)
    param? param += ")" : "";
    this.andSearch = param;
    this.runSearch();
  }

  rangeAbove(columnName, key) {
    let range = this.rangeText.split(',');
    this.rangeText = key + "," + columnName + "," + range[2];
    this.getJson();
    this.getCount();
  }

  rangeBelow(columnName, key) {
    let range = this.rangeText.split(',');
    this.rangeText = range[0] + "," + columnName + "," + key;
    this.getJson();
    this.getCount();
  }

  get tbodyTemplate() {
    if (this.myArray.length) {
      this.footerHidden = false;
      if (this.myArray[0] == "Loading...") {
        return html`
          <tr>
            <td colspan="${this.columnNames.length}">
              <div class="d-flex justify-content-center">
                <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
              </div>
            </td>
          </tr>
        `;
      } else {
        let one = true;
        return html`
          ${this.myArray.map(row => html`
            <tr>
              ${this.columnNames.map(col => html`
                ${!this.hidden || this.hiddenColumns.indexOf(col) == -1? html`
                  <td>
                    <code style="color: #444;">
                      ${this.ifLinks.hasOwnProperty(col)? html`
                        ${console.log(one = true)}
                        ${Object.keys(this.ifLinks[col]).map(checkKey => html`
                          ${one && row[checkKey] !== undefined && row[checkKey] != ""? html`
                            <a href="${this.ifLinks[col][checkKey].url}${row[this.ifLinks[col][checkKey].key]}" target="_blank">${row[col]}</a>
                            ${console.log(one = false)}
                          `:html``}
                        `)}
                      `:html`
                        ${this.links.hasOwnProperty(col)? html`
                          ${this.columns[col].type == "array" || this.columns[col].type == "Array"? html`
                            ${row[col][0]? html`
                              <table class="table table-sm" style="margin-bottom: 0;">
                                <tbody>
                                  ${row[col].map(value => html`
                                    <tr>
                                      <td scope="row"><a href="${this.links[col].url}${value}" target="_blank">${value}</a></td>
                                    </tr>
                                  `)}
                                </tbody>
                              </table>
                            `:html`
                              No data
                            `}
                          `:html`
                            ${this.columns[col].type == "hash" || this.columns[col].type == "Hash"? html`
                              ${row[col][0]? html`
                                <table class="table table-sm" style="margin-bottom: 0;">
                                  <tbody>
                                    ${row[col].map(value => html`
                                      <tr>
                                        ${value['id'] != ""? html`
                                          <td scope="row"><a href="${this.links[col].url}${value['id']}" target="_blank">${value['label']}</a></td>
                                          `:html`
                                            <td scope="row">${value['label']}</td>
                                          `}
                                      </tr>
                                    `)}
                                  </tbody>
                                </table>
                              `:html`
                                No data
                              `}
                            `:html`
                              ${row[col]? html`
                                ${this.columns[col].type == "str"? html`
                                  <a href="${this.links[col].url}${row[this.links[col].key]}" target="_blank">${row[col].substr(0, 10) + "..."}</a>
                                `:html`
                                  ${this.columns[col].type == "image" || this.columns[col].type == "Image"? html`
                                    <glytoucan-image imageFile="${this.imageFile}" accession="${row[col]}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
                                  `:html`
                                    <a href="${this.links[col].url}${row[this.links[col].key]}" target="_blank">${row[col]}</a>
                                  `}
                                `}
                              `:html`
                                No data
                              `}
                            `}
                          `}
                        `:html`
                          ${this.columns[col].type == "array" || this.columns[col].type == "Array"? html`
                            ${row[col][0]? html`
                              <table class="table table-sm" style="margin-bottom: 0;">
                                <tbody>
                                  ${row[col].map(value => html`
                                    <tr>
                                      <td scope="row">${value}</td>
                                    </tr>
                                  `)}
                                </tbody>
                              </table>
                            `:html`
                              No data
                            `}
                          `:html`
                            ${row[col]? html`
                              ${this.columns[col].type == "str"? html`
                                ${row[col].substr(0, 10) + "..."}
                              `:html`
                                ${this.columns[col].type == "image" || this.columns[col].type == "Image"? html`
                                  <glytoucan-image imageFile="${this.imageFile}" accession="${row[col]}" imageApi=${this.imageApi} host="${this.host}"></glytoucan-image>
                                `:html`
                                  ${this.columns[col].type == "JSON"? html`
                                    ${this.jsonTable((JSON.parse(row[col])))}
                                  `:html`
                                    <div style="white-space: pre-wrap;">${row[col]}</div>
                                  `}
                                `}
                              `}
                            `:html`
                              No data
                            `}
                          `}
                        `}
                      `}
                    </code>
                  </td>
                `:``}
              `)}
            </tr>
          `)}
        `;
      }
    } else {
      this.footerHidden = true;
      return html`
        <tr>
          <td colspan="${this.columnNames.length}" class="text-center">
            <div class="py-3">Data not found</div>
          </td>
        </tr>
      `;
    }
  }

  jsonTable(jsonObject) {
    let keys = Object.keys(jsonObject);
    return html`
      <table class="table table-sm" style="margin: -0.3rem;">
        <tbody>
          ${keys.map(key => html`
            <tr>
              <td scope="row">${key}</td>
              <td>${jsonObject[key]}</td>
            </tr>
          `)}
        </tbody>
      </table>
    `;
  }

  get infoTemplate() {
    if (this.count) {
      return html`
        Showing ${this.offset + 1} to ${this.offset + this.limit > this.count? html`
          ${this.count}
        `:html`
          ${this.offset + this.limit}
        `} of ${this.count} entries
      `;
    } else {
      return html`
        loading...
      `;
    }
  }

  get downloadTemplate() {
    if (this.myArray[0] != "Loading...") {
      return html`
        <div class="d-flex flex-column mx-2">
          <div class="btn-group mb-1" role="group" aria-label="Basic example">
            <button type="button" class="btn btn-outline-secondary" @click="${() => { this.downloadText(this.myArray); }}">Download the displayed table (.tsv)</button>
            <button type="button" class="btn btn-outline-secondary" @click="${() => { this.downloadAll(); }}">
              ${this.search? html`
                Download the search results (.tsv)
              `:html`
                Download all (.tsv)
              `}
            </button>
          </div>
          <small class="text-muted">Line breaks are removed from cells.</small>
        </div>
      `;
    }
  }

  downloadText(json) {
    const text = this.getText(json, "\t");
    let link = document.createElement('a')
    link.href = URL.createObjectURL(new Blob([text], {type: 'text/tab-separated-values'}));
    link.download = 'table.tsv'
    link.click()
  }

  getText(json, delimiter) {
    const header = Object.keys(json[0]).join(delimiter) + "\n";
    const body = json.map(function(d){
      return Object.keys(d).map(function(key){
        if (typeof(d[key]) == "object" || typeof(d[key]) == "number") {
          return d[key];
        } else {
          return d[key].replace(/\r?\n|\r/g, ' ');
        }
      }).join(delimiter);
    }).join("\n");
    return header + body;
  }

  async downloadAll() {
    this.load = "Loading...";
    let order;
    this.orderBool? order = "ASC" : order = "DESC";
    let newCol;
    if (this.columns[this.orderCol].type == "number" || this.columns[this.orderCol].type == "date") {
      newCol = "%3F" + this.orderCol;
    } else {
      newCol = "lcase(%3F" + this.orderCol + ")";
    }
    let searchText;
    this.numString? searchText = "&search=" + this.search : searchText = "";
    await fetch(this.listSparqlet + this.paramBreak + "order=" + order + "(" + newCol + ")&limit=&offset=0" + searchText).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.downloadText(myJson);
    });
    this.load = "";
  }

  get pageTemplate() {
    if (this.count) {
      const maxPage = Math.ceil(this.count / this.limit);
      const pointPage = maxPage - this.page;
      return html`
        <nav aria-label="Page navigation">
          <ul class="pagination" style="margin-bottom: -1rem;">
            ${this.page == 1? html`
              <li class="page-item disabled"><button class="page-link" tabindex="-1" aria-disabled="true">Previous</button></li>
            `:html`
              <li class="page-item"><button class="page-link" @click="${() => { this.page--; this.getJson(); }}">Previous</button></li>
              ${this.page < 5? html`
                ${this.beforeRepart(this.page - 1)}
              `:html`
                <li class="page-item"><button class="page-link" value="1" @click="${(e) => { this.transitionPage(e.target.value); }}}">1</button></li>
                <li class="page-item disabled"><span class="page-link">...</span></li>
                ${this.page <= maxPage - 4? html`
                  ${this.beforeRepart(2)}
                `:html`
                  ${this.beforeRepart(4 - pointPage)}
                `}
              `}
            `}
            <li class="page-item active" aria-current="page"><span class="page-link">${this.page}<span class="sr-only">(current)</span></span></li>
            ${this.page == maxPage? html`
              <li class="page-item disabled"><button class="page-link" tabindex="-1" aria-disabled="true">Next</button></li>
            `:html`
              ${this.page <= maxPage - 4? html`
                ${this.page < 5? html`
                  ${this.afterRepart(5 - this.page)}
                `:html`
                  ${this.afterRepart(2)}
                `}
                <li class="page-item disabled"><span class="page-link">...</span></li>
                <li class="page-item"><button class="page-link" value="${maxPage}" @click="${(e) => { this.transitionPage(e.target.value); }}}">${maxPage}</button></li>
              `:html`
                ${this.afterRepart(pointPage)}
              `}
              <li class="page-item"><button class="page-link" @click="${() => { this.page++; this.getJson(); }}">Next</button></li>
            `}
          </ul>
        </nav>
      `;
    } else {
       return html`
         <div class="spinner-border" role="status">
           <span class="sr-only">Loading...</span>
         </div>
      `;
    }
  }

  beforeRepart(range) {
    return html`
      ${[...Array(range).keys()].reverse().map(i => ++i).map(i => html`
        <li class="page-item"><button class="page-link" value="${this.page - i}" @click="${(e) => { this.transitionPage(e.target.value); }}">${this.page - i}</button></li>
      `)}
    `;
  }

  afterRepart(range) {
    return html`
      ${[...Array(range).keys()].map(i => ++i).map(i => html`
        <li class="page-item"><button class="page-link" value="${this.page + i}" @click="${(e) => { this.transitionPage(e.target.value); }}}">${this.page + i}</button></li>
      `)}
    `;
  }

  transitionPage(pageNumber) {
    this.page = Number(pageNumber);
    this.getJson();
  }
}

customElements.define("glycosmos-table", GlycosmosTable);
