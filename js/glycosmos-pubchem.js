import { LitElement, html } from "lit-element";
import { unsafeHTML } from 'lit-html/directives/unsafe-html.js';

class GlycosmosPubchem extends LitElement {
  static get properties() {
    return {
      domain: { type: String },
      variable: { type: String },
      myObject: { type: Object, reflect: true },
      diseaseArray: { type: Array, reflect: true },
      coArray: { type: Array, reflect: true }
    };
  }
  constructor() {
    super();
    this.domain = "https://test.sparqlist.glycosmos.org/sparqlist/api/";
    this.variable = "10020";
  }
  connectedCallback() {
    super.connectedCallback();
    this.getJson();
  }

  getJson() {
    this.myObject = ["Loading..."];
    let url = this.domain + "glycosmos_glycogenes_pubchem_disease?id=" + this.variable;
    fetch(url).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.myObject = myJson;
      this.diseaseArray = Object.keys(this.myObject);
    });
    this.coArray = ["Loading..."];
    let coccurrences = this.domain + "glycosmos_glycogenes_pubchem_disease_co?id=" + this.variable;
    fetch(coccurrences).then((response) => {
      return response.json();
    }).then((myJson) => {
      this.coArray = myJson;
    });
  }

  render() {
    return html`
      <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.4.1/pulse/bootstrap.min.css" rel="stylesheet" integrity="sha384-FnujoHKLiA0lyWE/5kNhcd8lfMILbUAZFAT89u11OhZI7Gt135tk3bGYVBC2xmJ5" crossorigin="anonymous">
      <div class="card">
        <div class="card-header">PubChem</div>
        <div class="card-body">
          ${this.cbodyTemplate}
          ${this.coTemplate}
        </div>
      </div>
    `;
  }

  get cbodyTemplate() {
    if (Object.keys(this.myObject).length) {
      if (this.myObject == "Loading...") {
        return html`
          <div class="d-flex justify-content-center">
            <div class="spinner-border" style="width: 5rem; height: 5rem;" role="status">
              <span class="sr-only">Loading...</span>
            </div>
          </div>
        `;
      } else {
        return html`
            <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Database</th>
                    <th scope="col">Disease</th>
                  </tr>
                </thead>
                ${this.diseaseArray.map((db) => html`
                  ${Object.keys(this.myObject).indexOf(db) !== -1? html`
                    <tbody>
                      <td>${db}</td>
                      <td>
                      ${this.myObject[db]['list'].map(row => html`
                        <a href="${row['url']}" target="_blank">${row['disease_label']}&nbsp;&nbsp;</a><br>
                      `)}
                      </td>
                    </tbody>
                  `:``}
                  `)}
              </table>
            </div>
        `;
      }
    } else {
      return html`
      <div class="py-3">Data not found</div>
      `;
    }
  }

  get coTemplate() {
    if (this.coArray.length) {
      if (this.coArray[0] == "Loading...") {
        return html`
        <span class="sr-only">Loading...</span>
        `;
      }else{
        return html`
          <li><a href="https://pubchem.ncbi.nlm.nih.gov/gene/${this.variable}#section=Gene-Disease-Co-Occurrences-in-Literature" target="_blank">Gene-Disease-Co-Occurrences-in-Literature(PubChem)</a></li>
        `;
      }
    }
  }
}

customElements.define("glycosmos-pubchem", GlycosmosPubchem);
